# Pair Programming Notes

1. Why pair program? What benefits do the videos claim for the practice?

    One of the benefits of pair programming is that it is less likely that you will make a silly mistake. If you have another programmer by your side at all times, they may notice when you make a small mistake. Another benefit is that it becomes a lot easier to ask for help when you need it. With someone right next to you, there is less pressure to get everything right first try.

2. What are the two distinct roles used in the practice of pair programming? How does having only one computer for two programmers aide in the pair adopting these roles?

    The two distinct roles used in pair programming are the driver and the navigator. The driver is the person who actually writes out all the code and the navigator is the one who keeps the driver on task and helps guide them in the right direction. Having only one computer aides in these roles because it forces the two programmers to work together instead of being off on their own. This means they are both less likely to make simple mistakes.
