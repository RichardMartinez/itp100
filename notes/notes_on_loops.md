# Lesson 6 Notes

1. Dr. Chuck calls looping the 4th basic pattern of computer programming. What are the other three patterns?

    The other three paterns that Dr. Chuck describes are sequential, conditional, and store and reuse. These four patterns are some of the most basic building blocks of Python. 

2. Describe the program Chr. Chuck uses to introduce the while loop. What does it do?

    Dr. Chuck introduces the while loop with a program that counts down from a given value n, then prints 'Blastoff!'. The loop checks if n > 0, then prints that number and subtracts one. This process is repeated until n = 0 and the loop exits and prints 'Blastoff!'.

3. What is another word for looping introduced in the video?

    The other word for looping that Dr. Chuck uses is 'iterates'. This basically just means passing over each value in succession.

4. Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?

    This kind of loop is called an infinite loop. This loop is 'broken' because it will never actually finish. It will simply repeat over and over forever because the condition that will make the loop stop will never be met.

5. What Python statement (a new keyword for us, by the way) can be used to exit a loop?

    The Python statement that can be used to exit a loop is the break statement. When a loop reaches this statement, it skips the rest of the loop and goes to the next line outside of the loop body. Basically, the break statement quits the loop and moves on with the next lines of code.

6. What is the other loop control statement introduced in the first video? What does it do?

    The other loop control statement is the continue statement. This statement, unlike the break statement, restarts the loop from the beginning. Basically, when a loop reaches a continue statement, it stops what it is doing and starts again from its first line of code.

7. Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is the type of loop he says will be discussed next?

    Dr. Chuck describes the while loop as an indefinite loop. It is called that because the while loop will keep iterating forever until it returns False. This means it has the possiblity to become and infinite loop. The next type of loop Dr. Chuck says he will discuss is the definite loop.

8. Which new Python loop is introduced in the second video? How does it work?

    The Python loop introduced in the second video is the for loop. This type of loop iterates over a given sequence until it reaches the end. For example, if given the list [1, 3, 4, 5, 7], the for loop will run the loop for each value in the sequence in order.

9. The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples of loop idioms does he introduce in this and the fourth video?

    Dr. Chuck describes loop idioms as patterns that have to do with how we construct loops. By using loop idioms, you can create a loop that actually completes a task. Dr. Chuck introduces a few loop idioms such as finding the largest number, counting in a loop, summing in a loop, finding the average, finding the smallest value, and a few more.
