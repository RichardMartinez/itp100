# Notes on Regex

1. What are regular expressions? What are they used for? When did they originate?

    Regular Expressions is a character based language that allows you to "smart search" through a text file. Instead of having keywords, RegEx has key characters that all mean something. If you want to search through some text, RegEx is a fast way to find exactly what you are looking for. RegEx was invented in the 1950s by American mathematician Stephen Kleene.

2. Use Markdown to reproduce the Regular Expression Quick Guide slide.

^       Matches the beginning of a line

$       Matches the end of the line

.       Matches any character

\s      Matches whitespace

\S      Matches any non-whitespace character

\*       Repeats a character zero or more times

*?      Repeats a character zero or more times (non-greedy)

\+       Repeats a character one or more times

+?      Repeats a character one or more times (non-greedy)

[aeiou] Matches a single character in the listed set

[^XYZ]  Matches a single character not in the listed set

[a-z0-9]The set of character can include a range

{       Indicates where string extraction is to start

}       Indicates where string extraction is to end

3. Make a text file with 10 lines in it. Write a python program that reads the contents of the file into a list of lines, and create a regular expression, rexp that will select exactly three items from lines.

    Data text file and regExTest.py can be found in exercises/regEx/ directory.

4. Do a web search and find a few good resources for using regular expressions in vim.

    The first two results on Google for "regular expression in vim" are:

    [**vimregex.com**](http://vimregex.com/)

    [**learnbyexample.gitbooks.io**](https://learnbyexample.gitbooks.io/vim-reference/content/Regular_Expressions.html)
