# Notes on Files

1. What function does Python use for accessing files? What arguments does it need? What does it return?

    Python uses the open() functions for accessing files. The arguments that it needs are the filename and the mode. The open() function returns a handle used to manipulate the file. Filename is a string and mode is optional. For mode, 'r' means read and 'w' means write.

2. What is a file handle? Explain how it relates to a file? Where is the data stored?

    Dr. Chuck describes a file handle as "a little opening outside the file that you can use to talk to the main file". The handle is not the file itself nor is it the data in that file. It is just a way for you to get to that file and read or write something to it. The data is stored in the file itself, not in the file handle.

3. What is '\n'?

    '\n' is what is known as the "newline" character. When printing a string, if it reached the newline character, it will do just that: create a new line where the \n is and continue printing from there. For example: print('Hello\nWorld!') would return Hello then a new line then World! Another important thing to note is that '\n' counts as one character, not two. So if you call len('X\nY'), it would return 3 because X is a character, \n is another, and Y is the third.

4. What does Dr. Chuck say is the most common way to treat a file when reading it?

    Dr. Chuck says the most common way to treat a file when reading it is to treat it as a long string with newline characters in it. Each new line in a text file isn't actually its own thing, it is simply part of a larger string that is separated into two different lines when you reach a newline character.

5. In the Searching Through a File (fixed) example, Dr. Chuck talks about the problem of the extra newline character that appears when we print out each line. He resolves this problem by using line.rstrip(), invoking Python's built-in rstrip method of strings. Could we also use a slice here, and write line[:-1] instead? Explain your answer.

    After testing it out in the Python shell, removing a newline character using slicing does appear to work. Slicing using line[:-1] removes the last character in the string. In this case, the last character would be the newline character. So when you slice away the last character, it removes the newline.    

6. The second video presents three different ways, or patterns for selecting lines that start with 'From:'. Compare these three patterns, providing examples of each.

    The first method Dr. Chuck uses is by using the .startswith method for strings. If the line starts with the given string 'From:', then the if statement will be True and the line will be printed.
    The second method Dr. Chuck uses to select the lines that start with 'From:' is by using the continue statement to skip bad lines. For example, in Dr. Chuck's program, he writes: if not line.startswith('From:'): continue. This means that if the line does not start with 'From:', the loop would restart skipping the print statement at the end.
    The third method Dr. Chuck uses is using the in operator. In his loop, Dr. Chuck writes: if not '@uct.ac.za' in line: continue. This basically checks the line as a string, and if the email address is not in that string, it would restart the loop skipping the print statement. This method kind of builds off the previous one.

7. A new Python construct is introduced at the end of the second video to deal with the situation when the program attempts to open a file that isn't there. Describe this new construct and the two new keywords that pair to make it.

   The new construct that is introduced at the end of the video is the trial construct. The two keywords that are paired to make it are "try" and "except". When you use a try statement, Python will run the code that follows it. If it returns an error, the program will move onto the except statement. If it does not return an error, the except statement is skipped and the program continues on the next line. 
