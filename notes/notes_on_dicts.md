# Notes on Dictionaries

1. Describe the characteristics of a collection.

    A collection is a variable that you can define to have multiple pieces of information stored within it rather than just a single value. This makes it very powerful because it basically creates a mini-database that can be reference throughout your program.

2. Using the video for inspiration, make a high level comparision of lists and dictionaries. How should you think about each of them?

    You should think of a list as a collection of values that must be in an order and must stay in that order. This is useful for when you want to have values in a given order. You should think of a dictionary, however, as a "grab bab" of values. Instead of specifying an order, each value in the dictionary has a "key". When you call for a value from the dictionary using its key, you can return that value.

3. What is the synonym Dr. Chuck tells us for dictionaries, that he presents in the slide showing a similiar feature in other programming languages?

    The synonym that Dr. Chuck tells us for dictionaries is "bags". This is because it makes sense to think about dictionaries as bags you can pull from rather than a list you must order.

4. Show a few examples of dictionary literals being assigned to variables.

    The example that Dr. Chuck shows in the video of a dictionary literal being assigned to a variable is >>> jjj = { 'chuck': 1 , 'fred': 42, 'jan': 100 }. This creates and assigns all the values specified to a dictionary called jjj. This can then be referenced later by calling jjj[string] where string is the key you want to pull out.

5. Describe the application in the second video which Dr. Chuck says is one of the most common uses of dictionaries.

    Dr. Chuck says one of the most common uses of dictionaries is for counting things. He says you can think about the counting kind of like a histogram. As you iterate through something, you can keep a running total of how many times you've seen that thing. You can then find the highest number to find the most common item.

6. Write down the code Dr. Chuck presents run this application.

    The application that Dr. Chuck presents takes in a list of names, counts how many times you see it, then outputs a dictionary with the number of times you see each name with the name itself as the "key".

7. What is the dictionary method that makes this common operation shorter (from 4 lines to 1)? Describe how to use it.

    The dictionary method that makes this operation shorter is the dict.get method. This method is called by using dict.get(key, 0). This basically does the same thing as Dr. Chuck's program, but does it all in one line. If the name exists, return the name, else create the name and set its value to 0.

8. Describe the application that Dr. Chuck leads into in the third video and codes in front of us in the fourth video.

    The application Dr. Chuck describes takes in a text file, and prints out the most common word in the file and exactly how many times it occurs. It uses all of the concepts we have learned up until this point. The program first creates a dictionary that acts as a histogram for the words. Then, it loops through that dictionary and returns then prints the highest count and the word.
