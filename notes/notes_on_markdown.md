# Lesson 5 Notes for ITP 100

These are my lesson 5 notes.

1. How can we use Markdown and git to create nice looking documents under revision control?

    Markdown is a lightweight way to display text. This makes it easy to inform the user of a program exactly what is
    going on and how to use your creation. In Git, any Markdown file will automatically be translated into HTML which
    will display on any webpage.

2. Dr. Chuck describes functions as "store and reused patterns" in programming. Explain what he means by this.

    When Dr. Chuck describes functions as "store and reused patterns", he means that when you assign some code to a
    function, it is easy to repeatedly apply that code in the future. For example, instead of writing the exact same
    code over and over for a bunch of different values, you can write a function with your code then simply type
    function() (or whatever exactly you called it) to easily run whatever code you needed.

3. How do we create functions in Python? What new keyword do we use? Provide your own example of a function written in Python.

    You can create a function in Python by using the def statement. For example, you could write:
        def function(x):
            x = 2 * x + 1
    This would create a function that evaulates f(x) = x^2 + 1 and returns the value back to x. Now, the next time you need to
    evaulate this expression, you can simply write function(x).
