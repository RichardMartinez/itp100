# Notes on Lists

1. In the first video of this lesson, Dr. Chuck discusses two very important concepts: algorithms and data structures. How does he define these two concepts? Which one does he say we have been focusing on until now?

    Dr. Chuck defines algorithms as a set of rules or steps used to solve a problem. He defines data structures as a particular way of organizing data in a computer. Up until this point, we have been focused on algorithms. An algorithm simply tells the computer a list of steps to follow in order to solve a problem. A data structure is just a clever way of organizing some data that can make a problem easier to solve.

2. Describe in your own words what a collection is.

    A collection is a group of values that can be stored in an easy to access place. Having a collection makes working with large groups of data much easier.

3. Dr. Chuck makes a very important point in the slide labeled Lists and definite loops - best pals about Python and variable names that he has made before, but which bares repeating. What is that point?

    Dr. Chuck makes the point that the variable name you choose for your variables does not convey any information to Python at all. Just because the iteration variable is called "friend" and the list is called "friends" does not mean Python knows exactly what you mean whenever you reference those variables. This can also cause issues on the side of the programmer because if you mistype and call "friend" instead of "friends", you can get a traceback error.

4. How do we access individual items in a list?

    You can access the individual items in a list by using an index. If you have a list called "friends", then you can reference the first item in that list by using "friends[0]". Or the second item by using "friends[1]".

5. What does mutable mean? Provide examples of mutable and immutable data types in Python.

    Mutable means that you can change what is stored inside a data structure. Lists are a mutable data type. For example, if you have a list "fruits" then you can replace the first item in that list by using >>> fruits[0] = "new_value". An immutable data type is strings. You cannot change the first letter of a string by using "string[0] = new_value". 

6. Discribe several list operations presented in the lecture.

    One list operation that Dr. Chuck presents in the second video is concetenation. Just like how you can add two strings together, you can also add two lists together. When you concatenate lists, a new list is created with all the elements of the previous two lists. Lists can also be sliced just like strings. If you have a list called "friends" using "friends[1:3]" will only have the second and third items in the list. It starts at index 1, and goes up to but not including index 3.

7. Discribe several list methods presented in the lecture.

    One list method is the append method. When you call list.append(), it will add the input as a new value at the end of the list. Another method is the pop method. This method will remove the input off the list. The sort method will of course sort the elements of the list in order of their values. If they are numbers, it will sort least to greatest, but if they are strings, it will sort based on lowercase first a-z, then uppercase a-z.

8. Discribe several useful functions that take lists as arguments presented in the lecture.

    The built in functions that Dr. Chuck shows us are the len, max, min, and sum functions. The len() function will return how many items are in the list. The max() and min() functions will return the maximum and minimum values in the list respectively. Finally, the sum() function will sum up all the items in a list.

9. The third video describes several methods that allow lists and strings to interoperate in very useful ways. Describe these.

    The method that Dr. Chuck introduces is the split method. This method takes a string and makes a list with every word in that string in order. For example >>> string = 'Good Morning USA' >>> list = 'string.split()' >>> print(list) would return ['Good', 'Morning', 'USA'].

10. What is a guardian pattern? Use at least one specific example in describing this important concept.

    A guardian pattern is a way for you to test something to see if it would return a valid output. For example, if you know your code will blow up if it receives a list with less than 3 items, you can create a guardian pattern that prevents the "dangerous" code from running if the input list has less than 3 items.
