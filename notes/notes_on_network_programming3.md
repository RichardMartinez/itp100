# Notes on Network Programming 3

1. What is ASCII? What are its limitations and why do we need to move beyond it?

    ASCII stands for American Standard Code for Information Interchange. ASCII is the character set containing all english letters and common symbols. It is limiting because it is sometimes impossible to write in other languages. We need to move beyond it because computers are becoming widely used all around the world not just in english speaking countries.

2. Which function in Python will give you the numeric value of a character (and thus its order in the list of characters)?

    The function in Python that gives you the numeric value of a character us the ord() function. Capital letters come first, then lower case letters.

3. Dr. Chuck says we move from a simple character set to a super complex character set. What is this super complex character set called?

    The super complex character set is called Unicode. This set has billions of characters from every language and character set. This is the most versatile character set because it can be used for any language in the world.

4. Describe bytes in Python 3 as presented in the video. Do a little web searching to find out more about Python bytes. Share something interesting that you find.

    Python bytes are basically how the computer stores a value. The more bytes the computer uses, the more space it needs to store that information. From searching "what are python bytes" on google, I found a method called bytes() which returns a bytes object. This bytes objects is a sequence of integers in the range [0, 256].

5. Break down the process of using .encode() and .decode methods on data we send over a network connection.

    The .encode() and .decode() methods are used for dealing with data that you send over the internet. You retrive the data using the .request method then you need to .decode() each line to be able to use it. You then .encode() it to resend it back over the web.
