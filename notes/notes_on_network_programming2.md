# Notes on Network Programming 2

1. What is an application protocol? List examples of specific application protocols listed in the lecture. Can you think of one besides HTTP that we have been using in our class regularly since the beginning of the year?

    An application protocol is an agreed upon method for transferring data that makes that data readable. This method needs to be universally agreed upon because if every single machine read data differently, it would be virtually impossible to send data between machines. Some protocols that we have been using since the beginning of the year are HTML and Git. 

2. Name the three parts of a URL. What does each part specify?

    The three parts of a URL are the protocol, host, and document. The protocol determines what the "rules of the road" should be on how to read the data. The host determines what machine you need to connect to, and the document is what specific file you should access.

3. What is the request/response cycle? What example does Dr. Chuck use to illustrate it and describe how it works?

    The request/response cycle is how data is transferred over the web. One machine makes a request for the data, and another machine responds. The example that Dr. Chuck uses to illustrate this is two simple HTML pages that just link to each other. When you click on one of the links, a request is sent for the next page, this request receives a response and the next page can be loaded.

4. What is the IETF? What is an RFC?

    IETF stands for the Internet Engineering Task Force. RFC stands for Request for Comments. The IETF is a group that is interested in improving the internet. They write/process RFC's which are the documentation for how a protocol should run.

5. In the video titled Worked Example: Sockets, Dr. Chuck tells us where to download a large collection of sameple programs he has available associated with the course. Where do we find these examples?

    Dr. Chuck tells us to download sample code from a file linked on the PY4E website. This file is a .zip file and will download as a directory called code3.

6. Try the telnet example that Dr. Chuck shows us in the Worked Example: Sockets video. Can you retrieve the document using telnet?

   Yes, I was able to retrieve the document. 
