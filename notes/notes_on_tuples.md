# Notes on Tuples

1. Dr. Chuck begins this lesson by stating that tuples are really a lot like lists. In what ways are tuples like lists? Do you agree with his statement that they are a lot alike?

    Tuples are like lists because you can call different parts of the tuple just like a list. For example, you can call for an index of a tuple using [] just like you can with a list. You can also print a tuple and apply common functions such as len() or max() just as you can on a list. I agree that tuples and lists are a lot alike.

2. How do tuples essentially differ from lists? Why do you think there is a need for this additional data type if it is similar to a list?

    Tuples differ from lists because they are immutable. This means that you can't change on specific value in a tuple after you create it. With a list, you can reassign say list[2] = 10 and it will change the value at index 2 to 10. With a tuple however, doing this will cause a traceback. This data type is necessary even though they are so similar to lists because they are a lot easier on system memory and are therefore more efficient.

3. Dr. Chuck says that tuple assignment is really cool (Your instructor completely agrees with him, btw). Describe what tuple assignment is, showing a few examples of it in use. Do you think it is really cool? Why or why not?

    By using tuples, you can basically collapse many assignment lines into one line. For example, if you do >>> (x, y) = (99, 90) then x will recieve 99 and y will receive 90. This makes assigning values to variables much easier. I think this is really cool because it allows for much smoother code that is easier to read.

4. Summarize what you learned from the second video in this lesson. Provide example code to make support what you describe.

    In the second video, we learn about how tuples can be used as a data type. You can create a list of tuples that can then be sorted using the sorted() function to easily sort data. If you pull from a dictionary, it will sort by key, but if you want it to sort by value, you can make a for loop that creates a new list of tuples but in reverse order. Basically, tuples can be used as an efficient data structure that can make a lot of programs easier to read and understand.

5. In the slide titled Even Shorter Version Dr. Chuck introduces list comprehensions. This is another really cool feature of Python. Did the example make sense to you? Do you think you understand what is going on?

    List comprehension basically allows you to procedurally change each item in a list. This means you can apply a function to each item in a list and have it return without needing a temporary variable. For example, [(v, k) for k, v in c.items] will return the list of tuples "c" but in reverse order. This makes a common idea in Python all possible in one line. I think I understand this concept and the example made sense.
