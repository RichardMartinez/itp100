# Notes on Strings

1. What does Dr. Chuck say about indexing strings? How does this operation work? Provide a few examples.

    Strings are indexed starting from the number 0 (not 1 like you may expect). For example, in the string 'banana', the letter 'b' is index 0, 'a' is index 1, 'n' is 2, etc. You can call for a specific index of a string by using square brackets []. For example, >>> fruit = 'banana' >>> print(fruit[0]) would return 'b'. 

2. Which Python function returns the number or characters in a string? Provide a few examples of it being used.

    len() is the Python function that returns the number of characters in a string. For example, len('banana') is 6 (not 5 which is the largest index). len() counts the actual number of characters in a string. Another example, len('Python is cool!') is 15 because the len() function also counts spaces and punctation.

3. Discuss the two ways Dr. Chuck shows to loop over the characters in a string. Provide an example of each.

    The first way Dr. Chuck shows to loop over a string is using a while loop. He creates a new variable called index and interates over each character of the string. The example he gave was >>> index = 0 >>> while index < len(fruit): he then wrote out exactly what he wanted to do with each element. The more elegant way of looping over the characters in a string is using for. Dr. Chuck shows us the example: >>> for letter in 'banana': print (letter) . This is a much simpler way to accomplish the same result.

4. What is slicing? Explain it in your own words and provide several examples of slicing at work. including examples using default values for start and end.

   Slicing is when you can remove parts of the string and only use what is left. For example, >>> s = 'Monty Python' >>> s[0:4] would return 'Mont'. Another example, >>> s[:8] would return 'Monty Py'. Using the default values, s[:] returns the full string 'Monty Python'.

5. What is concatenation as it applies to strings. Show an example.

    Concatenation is when you tack on one string to another. When you concatenate strings, it does not automatically adda space, it literally just puts the two strings together. For example, 'Hello' + 'World' returns 'HelloWorld'. If you want the space, you would have to add it yourself; 'Hello' + ' World' returns 'Hello World'. 

6. Dr. Chuck tells us that in can be used as a logical operator. Explain what this means and provide a few examples of its use this way with strings.

    A logical operator is a way to "test" certain things and have it return either True or False. Some exmaples of logical operators are ==, <=, >=, and !=. An example Dr. Chuck gives is >>> 'n' in 'banana' returns True; and >>> 'm' in 'banana' returns False. Basically, if the first string is contained anywhere in the second string, it will return True. It does not have to be a single letter either; >>> 'ana' in 'banana' would also return True.

7. What happens when you use comparison operators (>, <, >=, <=, ==, !=) with strings?

    When you use comparison operators with strings, it will return True or False based on both the letter and case. Dr. Chuck mentions that uppercase is less than lowercase. This means if your strings are not capitalized in a uniform way, it may mess with the comparison operators.

8. What is a method? Which string methods does Dr. Chuck show us? Provide several examples.

    A method is basically a built in function that you can call on a string. This means you can change the string in some way without actually changing the contents of the string. For example >>> greet = 'Hello Bob' >>> greet.lower() would return 'hello bob'. Methods even work on constants; >>> 'Hello There'.lower() would return 'hello there'. Another example of a method is .upper; >>> greet.upper() would return 'HELLO BOB'. Another useful one is .find which returns the index of a given string within a larger string. For example, >>> greet.find('llo') would return 2 because it is the index of the first letter of the string it found.

9. Define white space.

    White space is a part of a string that only has non-printing characters. This means things like spaces or tabs count as white space. However, it only counts at the very start and end of a string. Any spaces in between words will be kept. You can use the .lstrip and the .rstrip methods to automatically remove white space on either the left or right end of a string.

10. What is unicode?

    Unicode is an old class type in Python 2. A string was defined to be a unicode type if it contained international characters such as Asian, French, or Spanish characters. In Python 3, all strings are unicode which means it is a lot easier to deal with strings in Python 3 when compared to Python 2.
