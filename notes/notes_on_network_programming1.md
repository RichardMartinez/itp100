# Notes on Network Programming

1. Dr. Chuck mentions the architecture that runs our network. What is the name of this architecture?

    The architecture that runs the network is called Transport Control Protocol (TCP). It basically allows for computers to communicate with each other by sending packets back and forth.

2. Take a close look at the slide labelled Stack Connections. Which layer does Dr. Chuck say we will be looking at, assuming that one end of this layer can make a phone call to the corresponding layer on the other end? What are the names of the two lower level layers that we will be abstracting away?

    The layer that we assume can make a phone call to the corresponding layer on the other end is the transport layer. The two lower layers that we will be abstracting away are the link layer and the internet layer. These two layers are what take care of moving packets back and forth.

3. We will be assuming that there is a _ that goes from point A to point B. There is a _ running at each end of the connection. Fill in the blanks.

    We will be assuming that there is a nice reliable pipe that goes from point A to point B. There is a process running at each end of the connection.

4. Define Internet socket as discussed in the video.

    A socket is when one computer makes a phone call to other requesting some data. This request goes over the internet and is handled by the web server. The web server then sends back the data for the computer to use.

5. Define TCP port as discussed in the video.

    A TCP port is basically an extension that tells the server which process you want to talk to. Many common processes have TCP ports linked to them which makes them easy to find and work with.

6. At which network layer do sockets exist?

    Sockets exists on the transport layer of the network. This is the layer that we assumed could make a phone call to another computer.

7. Which network protocol is used by the World Wide Web? At which network layer does it operate?

    The World Wide Web uses HyperText Transfer Protocol (HTTP) as its main network protocol. This protocol operates on the application layer.
