import string
import sys

lower = string.ascii_lowercase
upper = string.ascii_uppercase

def apply_cipher(k, string):
    """
      >>> apply_cipher(13, "THE QUICK BROWN FOX JUMPED OVER THE LAZY DOGS")
      'GUR DHVPX OEBJA SBK WHZCRQ BIRE GUR YNML QBTF'
      >>> apply_cipher(13, "stop this line is not the end")
      'fgbc guvf yvar vf abg gur raq'
      >>> apply_cipher(20, "the quick brown fox jumped over the lazy dogs")
      'nby kocwe vliqh zir dogjyx ipyl nby futs xiam'
      >>> apply_cipher(20, "STOP this line is not the end either")
      'MNIJ nbcm fchy cm hin nby yhx ycnbyl'
      >>> apply_cipher(20, "stuff less than A !#$%&'()*+,-./")
      "mnozz fymm nbuh U !#$%&'()*+,-./"
    """
    output = ''
    for letter in string:
        if letter not in lower + upper: 
            output += letter
            continue

        alphabet = lower if letter in lower else upper

        index = alphabet.index(letter) 

        output += alphabet[(index + k) % 26]
    return output

def mode_switcher():
    mode = input('Do you want to read from a test data file? (Y/n) ')

    if mode.lower() == 'y' or mode == '' or mode.lower() == 'yes':
        return True
    elif mode.lower() == 'n' or mode.lower() == 'no':
        return False
    else:
        sys.exit('Please enter a valid mode. Enter defaults to Y.')

if __name__ == '__main__':
    import doctest
    doctest.testmod()
