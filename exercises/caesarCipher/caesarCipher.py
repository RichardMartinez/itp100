from tools import *
from os import system

# system('clear')

if mode_switcher():
    listID = input('Which file number should I read from? ')
    print(20 * '-')

    try:
        with open(f"sample{listID}.txt",'r') as f:
            key = int(f.readline())

            for line in f:
                line = line.rstrip()
                if line == 'STOP':
                    break
                else:
                    print(apply_cipher(key, line))
    except:
        sys.exit('Please enter a valid file number.')
else:
    string = input('What string should I use then? ')

    try:
        key = int(input('And what should the key be? '))
    except:
        sys.exit('Please enter a valid key.')

    print(20 * '-')
    print(apply_cipher(key, string))

