def num_even_digits(n):
    """
    >>> num_even_digits(123456)
    3
    >>> num_even_digits(2468)
    4
    >>> num_even_digits(1357)
    0
    >>> num_even_digits(2)
    1
    >>> num_even_digits(20)
    2
    """
    evenNums = '02468'
    output = ''
    for d in str(n): 
        if d in evenNums:
            output = output + d
    return len(output)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
