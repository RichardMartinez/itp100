def middle_value(a, b, c):
    """
        >>> middle_value(3, 8, 5)
        5
        >>> middle_value(6, 9, 4)
        6
        >>> middle_value(3, 7, 1)
        3
        >>> middle_value(7, 7, 7)
        7
    """
    middle = 0
    if a == b and b == c:
        middle = a
    elif (a >= b and c > a) or (a >= c and b > a):
        middle = a
    elif (b >= a and c > b) or (b >= c and a > b):
        middle = b
    elif (c >= b and a > c) or (c >= a and b > c):
        middle = c
    return middle


middle_value(7, 3, 10)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
