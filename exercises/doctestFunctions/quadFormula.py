import math


def quadFormula(A, B, C):
    """
      >>> quadFormula(2, -6, 7)
      []
      >>> quadFormula(-4, 6, -9)
      []
      >>> quadFormula(1, 4, 4)
      [-2]
      >>> quadFormula(-2, 3, 9)
      [-1.5, 3]
      >>> quadFormula(1, 0, -25)
      [-5, 5]
    """
    sol1 = (-B + math.sqrt(B ** 2 - 4 * A * C)) / (2 * A)
    sol2 = (-B - math.sqrt(B ** 2 - 4 * A * C)) / (2 * A)

    return [sol1, sol2]
    

if __name__ == '__main__':
    import doctest
    doctest.testmod()
