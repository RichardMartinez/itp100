def join_together(this, that):
    """
        >>> join_together('happy', 'birthday')
        'happybirthday'
        >>> join_together('cheese', 'cake')
        'cheesecake'
    """
    return this + that

if __name__ == "__main__":
    import doctest
    doctest.testmod()
