# Never got this version of findingAnagrams to work
# Makes dictionaries that count the number of letters
# But I was unable to get past that part

def add_dicts(d1, d2):
    d3 = {}
    for key in d1:
        if key in d2:
            d3[key] = d1[key] + d2[key]
        else:
            d3[key] = d1[key]
    for key in d2:
        if key not in d1:
            d3[key] = d2[key]
    return d3

dicts = []
with open("input_data1.txt", "r") as f:
    index = 0
    for line in f:
        line = line.rstrip()
        line = list(line)
        line_dict = {}
        for letter in line:
            if letter not in line_dict:
                line_dict[letter] = 1
            else:
                line_dict[letter] += 1
        line_dict.pop(' ', None)

        if index == 0:
            sentence = line_dict
        else:
            dicts.append(line_dict)
        index += 1
    index -= 1

def main():
    global dicts
    print("Input:", dicts)
    for i, d in list(enumerate(dicts))[:]:
        # print("Started new dict", dicts.index(d)+1)
        # print("Input:", dicts)
        skip = 1
        run = True
        while skip <= index-1 and run:
            # print("entered while loop", skip)
            if i + 1 + skip <= index:
                added = add_dicts(d, dicts[i+skip])
                skip += 1
            else:
                run = False
            if added == sentence:
                print(added)
                return True
            else:
                dicts.append(added)
                print("Output:", dicts)
    return False


print(main())

# print(dicts)
