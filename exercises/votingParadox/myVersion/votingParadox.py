from tools import *
import sys

listID = input('What data file do you want to read from? ')

path = f"input{listID}.txt"

try:
    with open(path, 'r') as f:
        pass
except:
    sys.exit('Please enter a valid file number.')

plural_res = plurality(path)
exhaustive_res = exhaustive(path)
prim12_res = primary(path, 1, 2)
prim23_res = primary(path, 2, 3)
prim13_res = primary(path, 1, 3)

plural_res = [a for (a, b) in plural_res]

dash = '-' * 30

# data = [plural_res, exhaustive_res, prim12_res, prim13_res, prim23_res]

titles = {
        0: 'Plurality Results:',
        1: 'Exhaustive Results:',
        2: '12 Primary:',
        3: '13 Primary:',
        4: '23 Primary:'}
        
results = {
        0: plural_res,
        1: exhaustive_res,
        2: prim12_res,
        3: prim13_res,
        4: prim23_res}

print(dash)
for i in range(5):
    print(titles[i])
    print(results[i])
    print(dash)

