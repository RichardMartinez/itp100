def find_val(d, string):
    if string == '>':
        return max(d, key=d.get)
    elif string == '<':
        return min(d, key=d.get)

def find_results(election, string='W'):
    if string == 'WL':
        winner, loser = find_val(election, '>'), find_val(election, '<')
        if winner == loser:
            loser = '-1'
        return winner, loser
    elif string == 'W':
        return find_val(election, '>')
 

def plurality(path):
    election = {}
    results = []
    with open(path, 'r') as f:
        for line in f:
            line = line.rstrip()

            if line == '-1 -1 -1':
                results.append(find_results(election, 'WL'))
                break

            if line == '0 0 0':
                results.append(find_results(election, 'WL'))
                election = {}
                continue

            line = line.split()

            election[line[0]] = election.get(line[0], 0) + 1
    
    return results

def exhaustive(path):
    election = {}
    results = []
    index = 0
    
    plurality_results = plurality(path)

    with open(path, 'r') as f:
        for line in f:
            line = line.rstrip()

            if line == '-1 -1 -1':
                results.append(find_results(election))
                break

            if line == '0 0 0':
                results.append(find_results(election))
                election = {}
                index += 1
                continue

            line = line.split()

            if line[0] == plurality_results[index][1]:
                election[line[1]] = election.get(line[1], 0) + 1
            else:
                election[line[0]] = election.get(line[0], 0) + 1
    
    return results

def primary_stage(path, a, b):
    election = {}
    results = []
    a, b = str(a), str(b)

    with open(path, 'r') as f:
        for line in f:
            line = line.rstrip()

            if line == '-1 -1 -1':
                results.append(find_results(election))
                break

            if line == '0 0 0':
                results.append(find_results(election))
                election = {}
                continue

            line = line.split()

            if line[0] != a and line[0] != b:
                election[line[1]] = election.get(line[1], 0) + 1
            else:
                election[line[0]] = election.get(line[0], 0) + 1
    
    return results

def primary(path, a, b):
   results = []
   a, b = str(a), str(b)
   stage1 = primary_stage(path, a, b)

   L = ['1', '2', '3']
   L.remove(a)
   L.remove(b)
   c = L[0]

   for index, winner in enumerate(stage1):
       final = primary_stage(path, winner, c)[index]
       results.append(final)

   return results

