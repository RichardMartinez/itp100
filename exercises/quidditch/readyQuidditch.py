listID = input('What registration list do you want to use? ')
file = open(f"registration_list{listID}.txt")

# Order: G, R, S, H
members = [0, 0, 0, 0]
houses = ['Gryffindor', 'Ravenclaw', 'Slytherin', 'Hufflepuff']
output = ''

for line in file:
    line = line.rstrip()
    for index, house in enumerate(houses):
        if line.endswith(house):
            members[index] += 1

for index, digit in enumerate(members):
    if digit < 7:
        output += f"{houses[index]} does not have enough players. Please add {7-digit} more.\n"
    if digit > 7:
        output += f"{houses[index]} has too many players. Please remove {digit-7} of them.\n"

if output != '':
    print(output, end="")
else:
    print("List complete, let's play quidditch!")
