# Software Design Coursework (ITP100)

This is the coursework for a dual enrolled course at the Arlington Career Center.

## Exercises:

This is where I put all of the class exercises that I have completed. Challenge problems and test functions are all organized into their own folders.

## Notes:

These are my notes I take from Dr. Chuck's Python for Everybody video series. These notes answer the questions posted on the ITP100 class website.

## Programs:

The directory contains all of my personal Python Projects I have been working on. Most of these files were written in PyCharm and took inspiration from the internet. I learn most of what I write here from looking things up online. The bigger projects are all organized into folders and made using the PyGame library.
