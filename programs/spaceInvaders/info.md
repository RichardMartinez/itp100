# Space Invaders
#### Using PyGame

This game is inspired by the classic arcade game Space Invaders. It was made following [**this**](https://youtu.be/Q-__8Xw9KTM) youtube tutorial. Use WASD to move and the Spacebar to shoot. If you miss an enemy, you lose a life. If your healthbar drops to 0, you lose a life and your health resets. Survive as long as possible without losing all your lives. Each wave, the enemy ships get faster and on the 3rd, 5th, and 7th waves, your gun gets upgraded to shoot faster. This is my first "complex" PyGame project and pulls all its assets from its own assets folder..
