# Space Invaders inspired shooter game using PyGame module
# Following a YouTube tutorial, not my original idea
# WASD to move, Space to shoot
# 1 missed enemy = 1 lost life
# Healthbar reaches 0 = 1 lost life

import pygame
import os
import sys
import random
pygame.font.init()

# Init Screen
WIDTH, HEIGHT = 750, 750
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Space Invaders")

# Load images
RED_SPACE_SHIP = pygame.image.load(os.path.join("assets", "pixel_ship_red_small.png"))
GREEN_SPACE_SHIP = pygame.image.load(os.path.join("assets", "pixel_ship_green_small.png"))
BLUE_SPACE_SHIP = pygame.image.load(os.path.join("assets", "pixel_ship_blue_small.png"))
YELLOW_SPACE_SHIP = pygame.image.load(os.path.join("assets", "pixel_ship_yellow.png"))

RED_LASER = pygame.image.load(os.path.join("assets", "pixel_laser_red.png"))
GREEN_LASER = pygame.image.load(os.path.join("assets", "pixel_laser_green.png"))
BLUE_LASER = pygame.image.load(os.path.join("assets", "pixel_laser_blue.png"))
YELLOW_LASER = pygame.image.load(os.path.join("assets", "pixel_laser_yellow.png"))

BG = pygame.transform.scale(pygame.image.load(os.path.join("assets", "background-black.png")), (WIDTH, HEIGHT))

WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
score = 0


# Laser Class
class Laser:
    def __init__(self, x, y, img):
        self.x = x
        self.y = y
        self.img = img
        self.mask = pygame.mask.from_surface(self.img)

    def draw(self, window):
        window.blit(self.img, (self.x, self.y))

    def move(self, vel):
        self.y += vel

    def off_screen(self):
        global HEIGHT
        return self.y >= HEIGHT or self.y < 0

    def collision(self, obj):
        return collide(self, obj)


# Ship Parent Class
class Ship:
    COOLDOWNMAX = 20

    def __init__(self, x, y, health=100):
        self.x = x
        self.y = y
        self.health = health
        self.ship_img = None
        self.laser_img = None
        self.lasers = []
        self.cooldown = 0

    def draw(self, window):
        window.blit(self.ship_img, (self.x, self.y))
        for laser in self.lasers:
            laser.draw(window)

    def get_width(self):
        return self.ship_img.get_width()

    def get_height(self):
        return self.ship_img.get_height()

    def shoot(self):
        if self.cooldown == 0:
            laser = Laser(self.x, self.y, self.laser_img)
            self.lasers.append(laser)
            self.cooldown = 1

    def move_lasers(self, vel, obj):
        self.cooldown_counter()
        for laser in self.lasers:
            laser.move(vel)
            if laser.off_screen():
                self.lasers.remove(laser)
            elif laser.collision(obj):
                obj.health -= 10
                self.lasers.remove(laser)

    def cooldown_counter(self):
        if self.cooldown >= self.COOLDOWNMAX:
            self.cooldown = 0
        elif self.cooldown > 0:
            self.cooldown += 1


# Player Class
class Player(Ship):
    def __init__(self, x=0, y=0, health=100):
        super().__init__(x, y, health)
        self.ship_img = YELLOW_SPACE_SHIP
        self.laser_img = YELLOW_LASER
        self.mask = pygame.mask.from_surface(self.ship_img)
        self.max_health = health
        self.spawn_center()

    def move_lasers(self, vel, objs):
        self.cooldown_counter()
        for laser in self.lasers[:]:
            laser.move(vel)
            if laser.off_screen():
                self.lasers.remove(laser)
            else:
                for obj in objs:
                    if laser.collision(obj):
                        objs.remove(obj)
                        global score
                        score += 1
                        if laser in self.lasers:
                            self.lasers.remove(laser)

    def draw(self, window):
        super().draw(window)
        self.healthbar(window)

    def spawn_center(self):
        self.x = int((WIDTH-self.get_width())/2)
        self.y = int(HEIGHT - self.get_height() - 30)

    def healthbar(self, window):
        pygame.draw.rect(window, RED, (self.x, self.y + self.ship_img.get_height() + 10, self.ship_img.get_width(), 10))
        pygame.draw.rect(window, GREEN, (self.x, self.y + self.ship_img.get_height() + 10, int(self.ship_img.get_width() * self.health/self.max_health), 10))


class Enemy(Ship):
    COLOR_MAP = {
        "red": (RED_SPACE_SHIP, RED_LASER),
        "green": (GREEN_SPACE_SHIP, GREEN_LASER),
        "blue": (BLUE_SPACE_SHIP, BLUE_LASER)
    }

    def __init__(self, x, y, color, health=100):
        super().__init__(x, y, health)
        self.ship_img, self.laser_img = self.COLOR_MAP[color]
        self.mask = pygame.mask.from_surface(self.ship_img)

    def move(self, vel):
        self.y += vel

    def shoot(self):
        if self.cooldown == 0:
            laser = Laser(self.x-20, self.y, self.laser_img)
            self.lasers.append(laser)
            self.cooldown = 1


def collide(obj1, obj2):
    offset_x = obj2.x - obj1.x
    offset_y = obj2.y - obj1.y
    return obj1.mask.overlap(obj2.mask, (offset_x, offset_y)) is not None


# Main Game Loop
def main():
    global score
    run = True
    FPS = 60
    level = 0
    lives = 5
    score = 0
    font = pygame.font.SysFont("comicsans", 50)

    enemy_list = []
    wave_len = 5
    # enemy_vel = 2
    laser_vel = 7

    player_vel = 5
    player = Player()

    clock = pygame.time.Clock()

    lost = False
    lost_count = 0

    def redraw_window():
        # Draw Background
        WIN.blit(BG, (0, 0))

        # Draw Text
        lives_label = font.render(f"Lives: {lives}", 1, WHITE)
        level_label = font.render(f"Level: {level}", 1, WHITE)
        score_label = font.render(f"Score: {score}", 1, WHITE)
        WIN.blit(lives_label, (10, 10))
        WIN.blit(level_label, (WIDTH - level_label.get_width() - 10, 10))
        WIN.blit(score_label, (int(WIDTH/2 - score_label.get_width()/2), 10))

        for e in enemy_list:
            e.draw(WIN)

        player.draw(WIN)

        if lost:
            lost_label = font.render("You Lost!!", 1, WHITE)
            WIN.blit(lost_label, (int(WIDTH/2 - lost_label.get_width()/2), int(HEIGHT/2 - lost_label.get_height()/2)))

        pygame.display.update()

    while run:
        clock.tick(FPS)
        redraw_window()

        enemy_vel = 2 * (1 + int(level/5))

        if player.health <= 0:
            lives -= 1
            player.health = player.max_health

        if lives <= 0:
            lost = True
            lost_count += 1

        if lost:
            if lost_count > FPS * 2:
                run = False
            else:
                continue

        if len(enemy_list) == 0:
            level += 1
            wave_len += 5
            for i in range(wave_len):
                enemy = Enemy(random.randrange(100, WIDTH-100), random.randrange(-1500, -100), random.choice(["red", "blue", "green"]))
                enemy_list.append(enemy)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_a] and player.x - player_vel > 0:  # left
            player.x -= player_vel
        if keys[pygame.K_d] and player.x + player_vel + player.get_width() < WIDTH:  # right
            player.x += player_vel
        if keys[pygame.K_w] and player.y - player_vel > 0:  # up
            player.y -= player_vel
        if keys[pygame.K_s] and player.y + player_vel + player.get_height() + 20 < HEIGHT:  # down
            player.y += player_vel
        if keys[pygame.K_SPACE]:
            player.shoot()

        for enemy in enemy_list[:]:
            enemy.move(enemy_vel)
            enemy.move_lasers(laser_vel, player)

            if random.randrange(0, 3*FPS) == 1:
                enemy.shoot()

            if collide(enemy, player):
                player.health -= 10
                enemy_list.remove(enemy)
            elif enemy.y + enemy.get_height() > HEIGHT:
                lives -= 1
                enemy_list.remove(enemy)

        if 3 <= level < 5:
            player.COOLDOWNMAX = 10
        elif 5 <= level < 7:
            player.COOLDOWNMAX = 5
        elif level >= 7:
            player.COOLDOWNMAX = 3

        player.move_lasers(-laser_vel, enemy_list)


def main_menu():
    font = pygame.font.SysFont("comicsans", 70)
    run = True
    while run:
        WIN.blit(BG, (0, 0))
        title_label = font.render("Press the mouse to begin...", 1, WHITE)
        WIN.blit(title_label, (int(WIDTH/2 - title_label.get_width()/2), 350))
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                main()
    pygame.quit()


main_menu()
