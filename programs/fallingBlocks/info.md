# Falling Blocks
#### Using PyGame

This game is my first ever project using PyGame. It was made following [**this**](https://youtu.be/-8n91btt5d8) youtube tutorial. It is a relatively simple game. Use the left and right arrows to move left and right. Dodge the falling blue blocks. Each dodged block is one point and every 20 points, the speed increases. Your score is shown at the bottom of the screen. When you lose, the game ends and your score is shown on screen.
