# Dodge the Falling Blocks Game
# Using PyGame Library
# Based on a YouTube tutorial, not my original idea

import pygame
import sys
import random
import time
pygame.init()

# Initialize Variables
# Screen Size
WIDTH = 800
HEIGHT = 600

# RGB Color Values
RED = (255, 0, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
BLACK = (0, 0, 0)

# Player Attributes
p_size = 50
p_x = int(WIDTH/2)
p_y = HEIGHT - 2 * p_size

# Enemy Attributes
e_size = 50
e_x = random.randint(0, WIDTH - e_size)
e_y = 0
e_list = [[e_x, e_y]]
SPEED = 10
spawnMult = (SPEED - 10) / 10

# Display Variables
score = 0
myFont = pygame.font.SysFont(None, 35)

# Draw Screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Falling Blocks')
clock = pygame.time.Clock()


# Set speed depending on score (faster at higher scores)
def set_level(score):
    global SPEED
    nums = [20, 40, 60, 80, 100]
    for n in nums:
        if n < score < n + 20:
            SPEED = 10 + (nums.index(n)+1) * 10


# Spawn up to 10 enemies at a time, but never more
def spawn_enemies(e_list):
    if len(e_list) < 10 and random.random() < 0.1 * (spawnMult + 1):
        x_new = random.randint(0, WIDTH - e_size)
        y_new = 0
        e_list.append([x_new, y_new])


# Drop enemies from the sky
def draw_enemies(e_list):
    for i, e in enumerate(e_list):
        pygame.draw.rect(screen, BLUE, (e[0], e[1], e_size, e_size))
        if 0 <= e[1] < HEIGHT:
            e[1] += SPEED
        else:
            e_list.pop(i)
            global score
            score += 1


# Test for Collisions
def detect_collision(p_x, p_y, e_list):
    for e in e_list:
        if p_x <= e[0] <= p_x + p_size or e[0] <= p_x <= e[0] + e_size:
            if p_y <= e[1] <= p_y + p_size or e[1] <= p_y <= e[1] + e_size:
                return True
    return False


# Make sure screen is updated each cycle
def update_screen():
    screen.fill(BLACK)
    spawn_enemies(e_list)
    draw_enemies(e_list)
    pygame.draw.rect(screen, RED, (p_x, p_y, p_size, p_size))
    message(text, YELLOW, WIDTH-150, HEIGHT-40)
    pygame.display.update()


# Print message on the screen
def message(msg, color, x, y):
    screen_text = myFont.render(msg, True, color)
    screen.blit(screen_text, [x, y])


# Start Game Loop
gameOver = False
while not gameOver:

    for event in pygame.event.get():

        # Quit when the window is closed
        if event.type == pygame.QUIT:
            sys.exit()

        # Detect Key Presses and Move Player
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT and p_x - p_size >= 0:
                p_x -= p_size
            elif event.key == pygame.K_RIGHT and p_x + 2 * p_size <= WIDTH:
                p_x += p_size

    # End game if collision occurs
    if detect_collision(p_x, p_y, e_list):
        update_screen()
        time.sleep(1)
        gameOver = True

    # Print score and update screen
    set_level(score)
    text = f"Score: {score}"
    update_screen()

    clock.tick(30)

# Game Over Screen
while gameOver:
    screen.fill(BLACK)
    message(f"You lose! Your score was: {score}", YELLOW, WIDTH-550, int(HEIGHT/2))
    pygame.display.update()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
