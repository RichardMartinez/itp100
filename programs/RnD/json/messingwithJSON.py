import json
# from pprint import pprint

# Open Infile and Read Data (Could potentially write custom fn to streamline)
path = "movie_1.txt"

json_file = open(path, "r", encoding="utf-8")

movie = json.load(json_file)

json_file.close()


# Modify Data as a Python Dictionary
# output = json.dumps(movie, ensure_ascii=False)

movie["new"] = "This is a new value"
movie["credits"]["MVP"] = "Richard Martinez"


# Open Outfile and Write Data
output_file = open("output.json", "w", encoding="utf-8")

json.dump(movie, output_file, ensure_ascii=False, indent=4)

output_file.close()
