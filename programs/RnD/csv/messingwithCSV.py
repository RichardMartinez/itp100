import csv
from datetime import datetime

path = "google_stock_data.csv"
return_path = "google_returns.csv"

# Read Results from Existing CSV File
f1 = open(path, newline="")
reader = csv.reader(f1)

header = next(reader)

data = []
for row in reader:
    # row = [Date, Open, High, Low, Close, Volume, Adj. Close]
    date = datetime.strptime(row[0], "%m/%d/%Y")
    open_price = float(row[1])
    high = float(row[2])
    low = float(row[3])
    close = float(row[4])
    volume = int(row[5])
    adj_close = float(row[6])

    data.append([date, open_price, high, low, close, volume, adj_close])

f1.close()

# Write Results to New File
f2 = open(return_path, "w", newline="")
writer = csv.writer(f2)

writer.writerow(["Date", "Return"])

for i in range(len(data) - 1):
    todays_row = data[i]
    todays_date = todays_row[0]
    todays_price = todays_row[-1]

    yesterdays_row = data[i+1]
    yesterdays_price = yesterdays_row[-1]

    daily_return = (todays_price - yesterdays_price) / yesterdays_price

    formatted_date = todays_date.strftime("%m/%d/%Y")
    formatted_return = round(daily_return, 5)

    writer.writerow([formatted_date, formatted_return])
