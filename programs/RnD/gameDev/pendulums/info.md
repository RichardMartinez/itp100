# Pendulums

I had an idea to work on a grappling hook for a game and wanted to know how to make a pendulum in pygame.
The file Pendulum_pygame.py was downloaded from the internet that shows the finished product and was not made by me.
I am currently trying to understand and replicate those pendulums in my own file myPendulum.py.

I downloaded the original file from [**this**](https://youtu.be/vlrVB5X_bcM) youtube video. Again this is not my program.