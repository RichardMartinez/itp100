import pygame
from engine import *
# print([pkg for pkg in dir() if not pkg.startswith("__")])

pygame.init()
pygame.mixer.pre_init(44100, -16, 2, 512)

# Constants
WIDTH, HEIGHT = 480, 270
center_pos = (WIDTH//2, HEIGHT//2)
FPS = 60

# Init Screen
window = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Template for New Projects")
clock = pygame.time.Clock()

my_spritesheet = Spritesheet("assets/duckspritesheet.png")
# print(my_spritesheet.tile_size)

gamemap = TileMap("assets/demo_level.csv", my_spritesheet)

mainmusic = Music("assets/firstloop.wav")
secondmusic = Music("assets/theme.wav", 0.05)


def main():
    run = True

    def redraw_window():
        window.fill(Color.sky_blue)

        gamemap.draw(window)

        pygame.display.update()

    while run:
        clock.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    mainmusic.play()
                if event.key == pygame.K_2:
                    secondmusic.play()
                if event.key == pygame.K_SPACE:
                    Music.reader.fadeout(1000)

            if event.type == pygame.KEYUP:
                pass

        # Held Keys
        # keys = pygame.key.get_pressed()

        redraw_window()


main()

pygame.quit()
