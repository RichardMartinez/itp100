import pygame
from engine import *

# Constants
WIDTH, HEIGHT = 480, 270
center_pos = (WIDTH//2, HEIGHT//2)
FPS = 60

# Init Screen
window = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Template for New Projects")
clock = pygame.time.Clock()


def main():
    run = True

    def redraw_window():
        window.fill(Color.white)

        pygame.display.update()

    while run:
        clock.tick(FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if event.type == pygame.KEYDOWN:
                pass

            if event.type == pygame.KEYUP:
                pass

            # Held Keys
            # keys = pygame.key.get_pressed()

        redraw_window()


main()

pygame.quit()
