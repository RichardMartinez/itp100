import pygame
from engine import Color
import math

# TO BE INTEGRATED LATER NOT READY YET


class Pendulum:
    def __init__(self, pos, img):
        self.img = img

        self.x = pos[0]
        self.y = pos[1]

        self.length = 100
        self.angle = 0
        self.vel = 0

    def draw(self, surface):
        self.animate()
        start_pos = anchor_point
        end_pos = (self.x, self.y)
        pygame.draw.line(surface, Color.black, start_pos, end_pos, 2)

        formatted_pos = (end_pos[0]-self.img.get_width()//2, end_pos[1]-self.img.get_height()//2)

        surface.blit(self.img, formatted_pos)

    def set_pos(self, pos):
        self.x, self.y = pos

        self.length = math.sqrt(math.pow(self.x - anchor_point[0], 2) + math.pow(self.y - anchor_point[1], 2))

        self.angle = math.asin((self.x - anchor_point[0]) / self.length)

    def animate(self):
        accel = -0.005 * math.sin(self.angle)
        damping_factor = 0.99
        self.vel += accel
        self.vel *= damping_factor
        self.angle += self.vel

        self.x = round(anchor_point[0] + self.length * math.sin(self.angle))
        self.y = round(anchor_point[1] + self.length * math.cos(self.angle))


# run = True
# while run:
#     clock.tick(60)
#
#     for event in pygame.event.get():
#         if event.type == pygame.QUIT:                    #
#             run = False
#
#         if event.type == pygame.MOUSEBUTTONDOWN:
#             if event.button == 1:
#                 mypend.set_pos(event.pos)
#
#         if event.type == pygame.KEYDOWN:
#             if event.key == pygame.K_a:
#                 if abs(mypend.vel) <= max_speed:
#                     mypend.vel -= 0.02
#             if event.key == pygame.K_d:
#                 if abs(mypend.vel) <= max_speed:
#                     mypend.vel += 0.02
#
#     keys = pygame.key.get_pressed()
#     if bool(keys[pygame.K_w]):
#         if mypend.length > 0:
#             mypend.length -= 3
#         else:
#             mypend.length = 0
#             mypend.vel = 0
#             mypend.angle = 0
#     if bool(keys[pygame.K_s]):
#         mypend.length += 3
#
#     window.fill(Color.white)
#
#     if mypend.length > 1:
#         pygame.draw.circle(window, Color.red, anchor_point, int(mypend.length), 1)
#
#     mypend.draw(window)
#
#     pygame.display.update()

