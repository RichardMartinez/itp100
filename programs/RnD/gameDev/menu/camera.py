import pygame
from abc import ABC, abstractmethod
# vec = pygame.math.Vector2
from engine import Vector as vec


class Camera:
    def __init__(self, game):
        self.game = game

        self.player = self.game.player
        self.offset = vec(0, 0)
        self.offset_float = vec(0, 0)
        self.CONSTANT = vec(-self.game.WIDTH//2, -self.game.HEIGHT//2)

        self.method = None

    def set_method(self, method):
        self.method = method

    def scroll(self):
        self.method.scroll()


class CamScroll(ABC):
    def __init__(self, game, camera):
        self.game = game

        self.camera = camera
        self.player = self.game.player

    @abstractmethod
    def scroll(self):
        pass


class Follow(CamScroll):
    def __init__(self, game, camera):
        CamScroll.__init__(self, game, camera)

    def scroll(self):
        self.camera.offset_float.x += (self.player.rect.x - self.camera.offset_float.x + self.camera.CONSTANT.x) / 10
        self.camera.offset_float.y += (self.player.rect.y - self.camera.offset_float.y + self.camera.CONSTANT.y) / 10
        self.camera.offset.x, self.camera.offset.y = int(self.camera.offset_float.x), int(self.camera.offset_float.y)


class Border(CamScroll):
    def __init__(self, game, camera):
        CamScroll.__init__(self, game, camera)

    def scroll(self):
        self.camera.offset_float.x += (self.player.rect.x - self.camera.offset_float.x + self.camera.CONSTANT.x) / 10
        self.camera.offset_float.y += (self.player.rect.y - self.camera.offset_float.y + self.camera.CONSTANT.y) / 10
        self.camera.offset.x, self.camera.offset.y = int(self.camera.offset_float.x), int(self.camera.offset_float.y)

        borders = {
            "x": (-300, self.game.WIDTH + 300),
            "y": (-100, self.game.HEIGHT + 100)}

        self.camera.offset.x = min(max(borders["x"][0], self.camera.offset.x), borders["x"][1] - self.game.WIDTH)
        self.camera.offset.y = min(max(borders["y"][0], self.camera.offset.y), borders["y"][1] - self.game.HEIGHT)


class Auto(CamScroll):
    def __init__(self, game, camera):
        CamScroll.__init__(self, game, camera)

    def scroll(self):
        self.camera.offset.x += 1
        self.camera.offset.y += 1
