import pygame
from engine import *


class Player(pygame.sprite.Sprite):
    def __init__(self, game):
        pygame.sprite.Sprite.__init__(self)
        self.game = game

        self.is_jumping, self.on_ground = False, False
        self.gravity, self.friction = 0.5, -0.12

        self.pos = Vector(self.game.center_pos[0], self.game.center_pos[1])
        self.vel = Vector(0, 0)
        self.acceleration = Vector(0, self.gravity)

        self.img = Spritesheet.load_img("player.png", 4)
        self.rect = self.img.get_rect(center=self.pos())

    # def draw(self):
    #     self.game.window.blit(self.img, self.rect)

    def update(self):
        self.horizontal_movement()
        self.vertical_movement()

        self.respect_borders()

    def horizontal_movement(self):
        self.acceleration.x = 0
        if self.game.keys["LEFT"]:
            self.acceleration.x -= 1
        if self.game.keys["RIGHT"]:
            self.acceleration.x += 1

        if (not (self.game.keys["LEFT"] or self.game.keys["RIGHT"])) or (self.game.keys["LEFT"] and self.game.keys["RIGHT"]):
            self.acceleration.x += self.vel.x * self.friction

        self.vel.x += self.acceleration.x * self.game.dt
        self.limit_vel(6)

        # Potentially make it - accel?
        self.pos.x += self.vel.x * self.game.dt + (self.acceleration.x * 0.5) * (self.game.dt ** 2)
        self.rect.x = self.pos.x

        self.acceleration.x = max(-5, min(self.acceleration.x, 5))
        # print(round(self.acceleration.y, 3), round(self.vel.y, 3), round(self.pos.y, 3))

    def vertical_movement(self):
        if self.game.keys["UP"]:
            self.jump()
        elif self.is_jumping and self.vel.y < 0:
            self.vel.y *= 0.25
            self.is_jumping = False

        self.vel.y += self.acceleration.y * self.game.dt
        if self.vel.y > 7:
            self.vel.y = 7
        self.pos.y += self.vel.y * self.game.dt + (self.acceleration.y * 0.5) * (self.game.dt ** 2)

        if self.pos.y > self.game.background.get_height():
            self.on_ground = True
            self.vel.y = 0
            self.pos.y = self.game.background.get_height()

        self.rect.bottom = self.pos.y

    def limit_vel(self, max_vel):
        self.vel.x = max(-max_vel, min(self.vel.x, max_vel))
        if abs(self.vel.x) < 0.01:
            self.vel.x = 0

    def jump(self):
        if self.on_ground:
            self.is_jumping = True
            self.vel.y -= 12
            self.on_ground = False

    def respect_borders(self):
        if self.pos.x - self.game.camera.offset.x <= 0:
            self.pos.x = self.game.camera.offset.x
            self.vel.x, self.acceleration.x = 0, 0
        elif self.pos.x + self.rect.w - self.game.camera.offset.x >= self.game.WIDTH:
            self.pos.x = self.game.WIDTH + self.game.camera.offset.x - self.rect.w
            self.vel.x, self.acceleration.x = 0, 0

        if self.pos.y - self.game.camera.offset.y <= 0:
            self.pos.y = self.game.camera.offset.y
            self.vel.y = 0
        elif self.pos.y + self.rect.h - self.game.camera.offset.y >= self.game.HEIGHT:
            self.pos.y = self.game.HEIGHT + self.game.camera.offset.y - self.rect.h
            self.vel.y = 0
