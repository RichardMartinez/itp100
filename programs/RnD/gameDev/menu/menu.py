import pygame
from engine import *


class Menu:
    """
    Parent Menu class. Can be used to build a fully functional menu.
    """
    def __init__(self, game):
        self.game = game
        self.run_display = True
        self.cursor_rect = pygame.Rect(0, 0, 20, 20)

        self.state = ""
        self.meta_data = {}

        self.states = list(self.meta_data.keys())
        self.statics = []
        for name in []:
            self.states.remove(name)
            self.statics.append(name)

        self.cursor_rect.midtop = (0, 0)  # (self.meta_data[self.state][0] - 100, self.meta_data[self.state][1])

        self.button_index = 0  # self.states.index(self.state)

        self.background = pygame.transform.scale(Spritesheet.load_img("background-black.png"), (self.game.WIDTH, self.game.HEIGHT))

    def display_menu(self):
        """
        Main loop for the menu. This gets called when the it is the games current menu.
        """
        self.run_display = True
        while self.run_display:
            self.game.check_events(hold=False)

            self.check_input()

            self.redraw_window()

    def check_input(self):
        """
        Input logic for the menu.
        """
        pass
        # if self.game.keys["DOWN"]:
        #     pass
        # elif self.game.keys["UP"]:
        #     pass
        # elif self.game.keys["START"]:
        #     if self.state == "Start":
        #         self.game.playing = True
        #     elif self.state == "Options":
        #         pass
        #     elif self.state == "Credits":
        #         pass
        #     self.run_display = False

    def redraw_window(self):
        """
        Redraws the menu to the game window.
        """
        self.game.window.fill(Color.black)
        self.game.window.blit(self.background, (0, 0))

        for name in self.statics:
            pass
            self.game.draw_text(name, 15, self.meta_data[name], Color.sky_blue, centered=True)

        for name in self.states:
            pass
            if self.states.index(name) != self.button_index:
                self.game.draw_text(name, 15, self.meta_data[name], Color.red, centered=True)
            else:
                self.game.draw_text(name, 15, self.meta_data[name], Color.purple, centered=True)

        # Draw Cursor
        self.game.draw_text("*", 15, (self.cursor_rect.x, self.cursor_rect.y), Color.purple, centered=True)

        pygame.display.update()


class MainMenu(Menu):
    """
    Main Menu of the game. This should come before the main game loop.
    """
    def __init__(self, game):
        Menu.__init__(self, game)
        self.state = "Start"
        self.meta_data = {
            "Main Menu": (Message.move_pos(self.game.center_pos, (0, -20))),
            "Start": (self.game.center_pos[0], self.game.center_pos[1] + 30),
            "Options": (self.game.center_pos[0], self.game.center_pos[1] + 60),
            "Credits": (self.game.center_pos[0], self.game.center_pos[1] + 90)}

        self.statics = ["Main Menu"]
        self.states = [state for state in list(self.meta_data.keys()) if state not in self.statics]

        # for name in self.statics:
        #     self.states.remove(name)

        self.cursor_rect.midtop = (self.meta_data[self.state][0] - 100, self.meta_data[self.state][1])

        self.button_index = self.states.index(self.state)

    def check_input(self):
        if self.game.keys["DOWN"]:
            self.button_index = (self.button_index + 1) % len(self.states)
            self.state = self.states[self.button_index]
            self.cursor_rect.y = self.meta_data[self.state][1]
        elif self.game.keys["UP"]:
            self.button_index = (self.button_index - 1) % len(self.states)
            self.state = self.states[self.button_index]
            self.cursor_rect.y = self.meta_data[self.state][1]
        elif self.game.keys["RETURN"]:
            if self.state == "Start":
                self.game.playing = True
            elif self.state == "Options":
                self.game.current_menu = self.game.menus["OPTIONS"]
            elif self.state == "Credits":
                self.game.current_menu = self.game.menus["CREDITS"]
            self.run_display = False


class OptionsMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)
        self.state = "Volume"
        self.meta_data = {
            "Options": (self.game.center_pos[0], self.game.center_pos[1] - 20),
            "Volume": (self.game.center_pos[0], self.game.center_pos[1] + 30),
            "Controls": (self.game.center_pos[0], self.game.center_pos[1] + 60)}

        self.states = list(self.meta_data.keys())
        self.statics = []
        for name in ["Options"]:
            self.states.remove(name)
            self.statics.append(name)

        self.cursor_rect.midtop = (self.meta_data[self.state][0] - 100, self.meta_data[self.state][1])

        self.button_index = self.states.index(self.state)

    def check_input(self):
        if self.game.keys["BACK"]:
            self.game.current_menu = self.game.menus["MAIN"]
            self.run_display = False
        if self.game.keys["DOWN"]:
            self.button_index = (self.button_index + 1) % len(self.states)
            self.state = self.states[self.button_index]
            self.cursor_rect.y = self.meta_data[self.state][1]
        elif self.game.keys["UP"]:
            self.button_index = (self.button_index - 1) % len(self.states)
            self.state = self.states[self.button_index]
            self.cursor_rect.y = self.meta_data[self.state][1]
        elif self.game.keys["RETURN"]:
            if self.state == "Volume":
                self.game.current_menu = self.game.menus["VOLUME"]
            # elif self.state == "Controls":
            #     pass
            self.run_display = False


class CreditsMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)

    def check_input(self):
        if self.game.keys["BACK"]:
            self.game.current_menu = self.game.menus["MAIN"]
            self.run_display = False

    def redraw_window(self):
        self.game.window.fill(Color.black)
        self.game.window.blit(self.background, (0, 0))

        self.game.draw_text("Made by Richard Martinez", 15, self.game.center_pos, Color.red, centered=True)

        pygame.display.update()


class VolumeMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)
        self.meta_data = {
            "Volume": (self.game.center_pos[0], self.game.center_pos[1] - 20),
            "Slider": (Message.move_pos(self.game.center_pos, (-50, 5)))}
        # Slider is 100 px long and 10 px tall

        self.statics = list(self.meta_data.keys())
        self.statics.remove("Slider")

        self.volume = self.game.music.volume

        self.slider_size = (100, 10)

        self.red_rect = pygame.Rect(self.meta_data["Slider"], self.slider_size)
        self.green_rect = pygame.Rect(self.meta_data["Slider"], (self.slider_size[0] * self.volume, self.slider_size[1]))

    def display_menu(self):
        """
        Main loop for the menu. This gets called when the it is the games current menu.
        """
        self.run_display = True
        while self.run_display:
            self.game.clock.tick(100)
            self.game.check_events(hold=True)

            self.check_input()

            self.game.music.set_volume(self.volume)

            self.redraw_window()

    def check_input(self):
        self.green_rect = pygame.Rect(self.meta_data["Slider"], (self.slider_size[0] * self.volume, self.slider_size[1]))

        if self.game.keys["BACK"]:
            self.game.current_menu = self.game.menus["OPTIONS"]
            self.run_display = False
        if self.game.keys["LEFT"]:
            if self.volume > 0:
                self.volume -= 0.01
        elif self.game.keys["RIGHT"]:
            if self.volume < 1:
                self.volume += 0.01
        # print(self.game.clock.get_fps())

        if abs(self.volume) < 0.01:
            self.volume = 0
        elif abs(self.volume - 1) < 0.01:
            self.volume = 1

    def redraw_window(self):
        """
        Redraws the menu to the game window.
        """
        self.game.window.fill(Color.black)
        self.game.window.blit(self.background, (0, 0))

        for name in self.statics:
            self.game.draw_text(name, 15, self.meta_data[name], Color.sky_blue, centered=True)

        # Draw Volume Bar
        pygame.draw.rect(self.game.window, Color.red, self.red_rect)
        pygame.draw.rect(self.game.window, Color.green, self.green_rect)

        pygame.display.update()
