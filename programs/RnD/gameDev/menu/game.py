import pygame
from engine import *
from menu import *
from player import Player
from camera import *


class Game:
    """
    Game class that runs a given game.
    Main element is the .game_loop method as that is where everything gets run from.
    """
    def __init__(self):
        # Init Pygame
        pygame.mixer.pre_init(44100, -16, 2, 512)

        pygame.init()
        pygame.font.init()
        pygame.mixer.init()

        # Setup Loop Variables and Keys
        self.running, self.playing = True, False
        # self.keys = {
        #     "UP": False, "DOWN": False, "START": False, "BACK": False,
        #     "LEFT": False, "RIGHT": False, "1": False, "2": False, "3": False,
        #     "W": False, "S": False}

        self.keys = {key: False for key in key_dict.keys()}

        # Init Screen
        scalar = 2
        self.WIDTH, self.HEIGHT = 480 * scalar, 270 * scalar
        self.center_pos = (self.WIDTH//2, self.HEIGHT//2)
        self.window = pygame.display.set_mode((self.WIDTH, self.HEIGHT))
        pygame.display.set_caption("Main Menu Test")
        self.clock = pygame.time.Clock()
        self.current_FPS = 0
        self.dt = 0

        # Init Player
        self.player = Player(self)

        # Init Assets
        self.background = pygame.transform.scale(Spritesheet.load_img("background-black.png"), (self.WIDTH, self.HEIGHT))
        self.font_path = "8-bit-hud.ttf"
        self.music = Music("meadows.wav")
        # self.music.play()
        self.spritesheet = Spritesheet("trainer_spritesheet.png")
        self.test_image = self.spritesheet.get_sprite("f_trainer1.png")

        # Init Camera
        self.camera = Camera(self)
        self.scroll_methods = {
            "FOLLOW": Follow(self, self.camera),
            "BORDER": Border(self, self.camera),
            "AUTO": Auto(self, self.camera)}
        self.camera.set_method(self.scroll_methods["BORDER"])

        # Init Menus
        self.menus = {
            "MAIN": MainMenu(self),
            "OPTIONS": OptionsMenu(self),
            "CREDITS": CreditsMenu(self),
            "VOLUME": VolumeMenu(self)}
        self.current_menu = self.menus["MAIN"]

        # Test Button
        button_pos = (self.WIDTH - self.test_image.get_width() - 20, 20)
        button_msg = Message("Test", Color.sky_blue, Message.generate_font(), centered=True)
        self.main_button = Button(button_pos, self.test_image, button_msg)
        self.button_rect = self.test_image.get_rect()
        self.button_rect.x, self.button_rect.y = button_pos

        # Past here is experimental
        self.fadeout = Fadeout(self.window)
        # self.countdown = Countdown(4 * 1000)

    def game_loop(self):
        """
        Main game loop for the game. All code you want to run should go in here.
        """
        while self.playing:
            self.dt = self.clock.tick(60) * 0.001 * 60
            self.current_FPS = self.clock.get_fps()
            self.check_events(hold=True)

            if self.keys["1"]:
                self.camera.set_method(self.scroll_methods["FOLLOW"])
            elif self.keys["2"]:
                self.camera.set_method(self.scroll_methods["AUTO"])
            elif self.keys["3"]:
                self.camera.set_method(self.scroll_methods["BORDER"])

            if self.keys["w"]:
                self.fadeout.fading_out = True
            elif self.keys["s"]:
                self.fadeout.fading_in = True
            self.fadeout.update()

            self.player.update()

            if self.keys["BACK"]:
                self.player.vel.x = 0
                self.playing = False

            # self.countdown.count()
            # print(self.countdown.now, self.countdown.last_updated)
            # print(pygame.time.get_ticks())

            self.redraw_window()

    def redraw_window(self):
        """
        Redraws the game's window from back to front. This should be the last thing in the game loop.
        """
        self.camera.scroll()

        self.window.fill(Color.black)
        self.window.blit(self.background, (0 - self.camera.offset.x, 0 - self.camera.offset.y))

        msg_pos = (self.center_pos[0], 50)

        self.draw_text(f"FPS: {round(self.current_FPS)}", 20, msg_pos, Color.sky_blue, centered=True)

        # temp_rect = pygame.Rect(self.player.rect.x - self.camera.offset.x, self.player.rect.y - self.camera.offset.y, 50, 50)
        # pygame.draw.rect(self.window, Color.red, temp_rect)

        self.window.blit(self.player.img, (self.player.rect.x - self.camera.offset.x, self.player.rect.y - self.camera.offset.y))

        self.main_button.draw(self.window)
        pygame.draw.rect(self.window, Color.red, self.button_rect, 2)

        self.fadeout.draw()

        pygame.display.update()

    def check_events(self, hold=True):
        """
        Checks all events and stores the keys in self.keys.
        Hold = True means key is True until let go.
        Hold = False means key is True for one frame
        """
        if not hold:
            self.keys = {key: False for key in key_dict.keys()}

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running, self.playing = False, False
                self.current_menu.run_display = False

            if event.type == pygame.KEYDOWN:
                for key, value in key_dict.items():
                    if event.key == value:
                        self.keys[key] = True

                if event.key == pygame.K_SPACE:
                    self.music.play()

            if hold:
                if event.type == pygame.KEYUP:
                    for key, value in key_dict.items():
                        if event.key == value:
                            self.keys[key] = False

            if self.main_button.event_handler(event) and self.playing:
                print("You pressed me!")

    def draw_text(self, text, size, pos, color, centered):
        """
        Draws a message onto the game window. Font is currently preset but could be changed later.
        Uses the pre-written Message class from engine.py.
        """
        font = Message.generate_font(self.font_path, size, sys=False)  # pygame.font.Font(self.font_path, size)
        msg = Message(text, color, font, centered)
        msg.display(self.window, pos)

    def pos_from_center(self, dx, dy):
        return Message.move_pos(self.center_pos, (dx, dy))
