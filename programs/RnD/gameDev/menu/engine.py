import pygame
import json
import csv


def init_pygame():
    pygame.init()
    pygame.font.init()
    pygame.mixer.pre_init(44100, -16, 2, 512)
    pygame.mixer.init()
    pygame.mixer.set_num_channels(64)


key_dict = {"SPACE": pygame.K_SPACE, "ESC": pygame.K_ESCAPE, "RETURN": pygame.K_RETURN, "BACK": pygame.K_BACKSPACE,
            "UP": pygame.K_UP, "DOWN": pygame.K_DOWN, "LEFT": pygame.K_LEFT, "RIGHT": pygame.K_RIGHT,

            "a": pygame.K_a, "b": pygame.K_b, "c": pygame.K_c, "d": pygame.K_d, "e": pygame.K_e, "f": pygame.K_f,
            "g": pygame.K_g, "h": pygame.K_h, "i": pygame.K_i, "j": pygame.K_j, "k": pygame.K_k, "l": pygame.K_l,
            "m": pygame.K_m, "n": pygame.K_n, "o": pygame.K_o, "p": pygame.K_p, "q": pygame.K_q, "r": pygame.K_r,
            "s": pygame.K_s, "t": pygame.K_t, "u": pygame.K_u, "v": pygame.K_v, "w": pygame.K_w, "x": pygame.K_x,
            "y": pygame.K_y, "z": pygame.K_z,

            "1": pygame.K_1, "2": pygame.K_2, "3": pygame.K_3, "4": pygame.K_4, "5": pygame.K_5, "6": pygame.K_6,
            "7": pygame.K_7, "8": pygame.K_8, "9": pygame.K_9, "0": pygame.K_0,

            "num0": pygame.K_KP0, "num1": pygame.K_KP1, "num2": pygame.K_KP2, "num3": pygame.K_KP3, "num4": pygame.K_KP4,
            "num5": pygame.K_KP5, "num6": pygame.K_KP6, "num7": pygame.K_KP7, "num8": pygame.K_KP8, "num9": pygame.K_KP9}


class Spritesheet:
    """
    Spritesheet parser class. Requires .png spritesheet and same name .json meta data.
    """
    def __init__(self, filename):
        self.filename = filename
        self.meta_data = filename.replace(".png", ".json")
        self.sprite_sheet = pygame.image.load(self.filename).convert()

        with open(self.meta_data) as f:
            self.data = json.load(f)

        first_item = list(self.data['frames'].keys())[0]
        self.tile_size = self.data['frames'][first_item]["sourceSize"]["w"]

    def get_sprite(self, name, scalar=1):
        """
        Returns a surface with the specified frame.
        Pulls from meta data .json file. Can scale an image up just like the load_img() static method.
        """
        sprite = self.data['frames'][name]['frame']
        x, y, w, h = sprite['x'], sprite['y'], sprite['w'], sprite['h']

        surface = pygame.Surface((w, h))
        surface.set_colorkey(Color.black)
        surface.blit(self.sprite_sheet, (0, 0), (x, y, w, h))

        surface = pygame.transform.scale(surface, (int(surface.get_width()*scalar), int(surface.get_height()*scalar)))
        return surface

    @staticmethod
    def load_img(path, scalar=1):
        """
        Load a single image if a spritesheet is not available.
        Use the scalar to change the size of the image if your pure assets are low res.
        """
        img = pygame.image.load(path).convert()
        img.set_colorkey(Color.black)
        img = pygame.transform.scale(img, (int(img.get_width() * scalar), int(img.get_height() * scalar)))
        return img


class Tile(pygame.sprite.Sprite):
    """
    Tile object that can be used to build a TileMap.
    Generally, tiles pull from a tileset Spritesheet, but functionality to pull from image may be added later.
    """
    def __init__(self, img, pos, spritesheet):
        pygame.sprite.Sprite.__init__(self)

        self.img = spritesheet.get_sprite(img)
        self.pos = pos
        self.rect = pygame.Rect(self.pos, (self.img.get_width(), self.img.get_height()))

    def draw(self, surface):
        surface.blit(self.img, self.pos)


class TileMap:
    """
    Read from a .csv file and make a surface with the given tiles. Can then be drawn onto the window.
    """
    def __init__(self, filename, spritesheet):
        self.start_x, self.start_y = 0, 0
        self.map_w, self.map_h = 0, 0
        self.spritesheet = spritesheet
        self.tile_size = self.spritesheet.tile_size

        self.tiles = self.load_tiles(filename)
        self.map_surface = pygame.Surface((self.map_w, self.map_h))
        self.map_surface.set_colorkey(Color.black)
        for tile in self.tiles:
            tile.draw(self.map_surface)
            # pygame.draw.rect(self.map_surface, Color.white, tile.rect)

    def draw(self, surface):
        """
        Draws the generated surface onto the window at the top left.
        """
        surface.blit(self.map_surface, (0, 0))

    @staticmethod
    def read_csv(filename):
        """
        Reads a level .csv and returns a list of lists of numbers that represent the tiles.
        """
        array = []
        with open(filename, "r") as f:
            data = csv.reader(f, delimiter=',')
            for row in data:
                array.append(list(row))
        return array

    def load_tiles(self, filename):
        """
        Using the .csv file, make a list of Tile objects that can then be iterated over.
        """
        tile_ids = {
            -1: None,
            0: "chick.png",
            1: "grass.png",
            2: "grass2.png"
        }

        tiles = []
        gamemap = self.read_csv(filename)
        x, y = 0, 0
        for row in gamemap:
            x = 0
            for tile in row:
                tile = int(tile)
                if tile != -1:
                    # self.start_x, self.start_y = x * self.tile_size, y * self.tile_size
                    tiles.append(Tile(tile_ids[tile], (x * self.tile_size, y * self.tile_size), self.spritesheet))
                x += 1
            y += 1

        self.map_w, self.map_h = x * self.tile_size, y * self.tile_size
        return tiles


class Button:
    """
    Builds a button on a surface with specified position, image, and optional message.
    """
    def __init__(self, pos, img, msg=None):
        self.pos = pos
        self.img = img
        self.rect = pygame.Rect(self.pos, (self.img.get_width(), self.img.get_height()))
        self.msg = msg

    def draw(self, surface):
        """
        Draws button and optional message onto the given surface.
        """
        surface.blit(self.img, self.pos)

        if self.msg:
            if self.msg.centered:
                msg_pos = self.rect.center
            else:
                msg_pos = self.pos

            self.msg.display(surface, msg_pos)

    def event_handler(self, event):
        """
        Handles the user clicking on the button. This will return True if the button is clicked.
        """
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                if self.rect.collidepoint(event.pos):
                    return True


class Message:
    """
    Contains all info needed for a message. Can either be printed directly using .display() or can be passed to a Button object.
    Color can pull from Color class, font can be generated using .generate_font() static method of the Message class.
    If centered = True, the message will be centered on the given position rather than being in the top left.
    """
    def __init__(self, text, color, font, centered=False):
        self.text = text
        self.color = color
        self.font = font
        self.label = self.font.render(self.text, True, self.color)
        self.centered = centered
        self.rect = self.label.get_rect()

    def display(self, surface, pos):
        """
        Displays message on a given surface at the position.
        If centered = True, the message will be centered on the given position rather than the top left.
        """
        if self.centered:
            self.rect.center = pos
            # pos = (pos[0]-self.label.get_width()//2, pos[1]-self.label.get_height()//2)
        else:
            self.rect.x, self.rect.y = pos
        surface.blit(self.label, self.rect)

    @staticmethod
    def generate_font(name=None, size=35, sys=True):
        """
        Returns a font object that can be used to write a message. Pass no arguments for default fonts.
        List of system fonts that are available can be found by using pygame.font.get_fonts()
        """
        if sys:
            return pygame.font.SysFont(name, size)
        return pygame.font.Font(name, size)

    @staticmethod
    def move_pos(pos, change):
        return pos[0] + change[0], pos[1] + change[1]


class Music:
    """
    A simple way to play music.
    This class just packages up pygame's built in music player into an easier pill to swallow.
    """
    reader = pygame.mixer.music

    def __init__(self, filename, volume=0.5):
        self.filename = filename
        self.volume = volume

    def play(self, loops=-1):
        self.stop()
        self.reader.load(self.filename)
        self.reader.set_volume(self.volume)
        self.reader.play(loops)

    def stop(self):
        self.reader.stop()

    def set_volume(self, n):
        self.volume = n
        self.reader.set_volume(n)

    # Add more methods later? (Stop, Pause, Continue, Fadeout)


# Idea for Sounds class to be added later
# class Sounds:
#     pass


class Fadeout:
    def __init__(self, window):
        self.window = window
        self.WIDTH = self.window.get_width()
        self.HEIGHT = self.window.get_height()

        self.alpha = 0

        self.surface = pygame.Surface((self.WIDTH, self.HEIGHT)).convert_alpha()
        self.surface.fill(Color.black)
        self.surface.set_alpha(self.alpha)

        self.countdown = Countdown(1)

        self.fading_out, self.fading_in = False, False

    def fadeout(self):
        if self.countdown.count() and self.alpha < 255:
            self.alpha += 5
        self.surface.set_alpha(self.alpha)

    def fadein(self):
        if self.countdown.count() and self.alpha > 0:
            self.alpha -= 5
        self.surface.set_alpha(self.alpha)

    def update(self):
        if self.fading_out:
            self.fadeout()
            if self.alpha >= 255:
                self.fading_out = False
        elif self.fading_in:
            self.fadein()
            if self.alpha <= 0:
                self.fading_in = False

    def draw(self):
        if self.alpha > 0:
            self.window.blit(self.surface, (0, 0))
            pygame.display.update()


class Countdown:
    def __init__(self, ms):
        self.now = pygame.time.get_ticks()
        self.last_updated = 0
        self.ms = ms

    def count(self):
        self.now = pygame.time.get_ticks()
        if self.now - self.last_updated > self.ms:
            self.last_updated = self.now
            return True
        return False


class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __call__(self, *args, **kwargs):
        return self.x, self.y


class Color:
    """
    Simple color container to avoid constants in main program. More colors can be added for easy use.
    """
    # Rainbow
    black = (0, 0, 0)
    white = (255, 255, 255)
    red = (255, 0, 0)
    orange = (255, 165, 0)
    yellow = (255, 255, 0)
    green = (0, 255, 0)
    blue = (0, 0, 255)
    purple = (128, 0, 128)

    # Unique
    sky_blue = (110, 222, 255)
