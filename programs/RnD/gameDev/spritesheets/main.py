import pygame
from tools import *

# Init Screen
pygame.init()
WIDTH, HEIGHT = 480, 270
window = pygame.display.set_mode((WIDTH, HEIGHT))

WHITE = (255, 255, 255)

pygame.display.set_caption("Spritesheets")

my_spritesheet = Spritesheet("trainer_spritesheet")

trainer = [my_spritesheet.get_sprite(f'trainer{x+1}') for x in range(5)]
f_trainer = [my_spritesheet.get_sprite(f'f_trainer{x+1}') for x in range(5)]
index = 0

run = True
while run:

    def redraw_window():
        window.fill(Color.white)

        window.blit(trainer[index], (0, HEIGHT-128))
        window.blit(f_trainer[index], (128, HEIGHT-128))
        pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                index = (index + 1) % len(trainer)

        redraw_window()
