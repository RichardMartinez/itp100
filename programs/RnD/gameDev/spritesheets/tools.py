import pygame
import json


class Spritesheet:
    def __init__(self, filename):
        self.filename = filename + ".png"
        self.meta_data = filename + ".json"
        self.sprite_sheet = pygame.image.load(self.filename).convert()

        with open(self.meta_data) as f:
            self.data = json.load(f)

    # def get_sprite(self, x, y, w, h):
    #     sprite = pygame.Surface((w, h))
    #     sprite.set_colorkey(Color.black)
    #     sprite.blit(self.sprite_sheet, (0, 0), (x, y, w, h))
    #     return sprite

    def get_sprite(self, name):
        sprite = self.data['frames'][name + ".png"]['frame']
        x, y, w, h = sprite['x'], sprite['y'], sprite['w'], sprite['h']
        surface = pygame.Surface((w, h))
        surface.set_colorkey(Color.black)
        surface.blit(self.sprite_sheet, (0, 0), (x, y, w, h))
        return surface


class Color:
    black = (0, 0, 0)
    white = (255, 255, 255)
    red = (255, 0, 0)
    orange = (255, 165, 0)
    yellow = (255, 255, 0)
    green = (0, 255, 0)
    blue = (0, 0, 255)
    purple = (128, 0, 128)
