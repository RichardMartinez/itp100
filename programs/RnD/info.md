# Research and Development (RnD)

This directory is where I can mess around with cool new things I find online. 
I find this information by searching the web and following tutorials.
I will continue to add on to this directory as I experiment with more ideas.

Links to Documentation I followed:  
[**bs4**](https://youtu.be/XQgXKtPSzUI)  
[**csv**](https://youtu.be/Xi52tx6phRU)  
[**json**](https://youtu.be/pTT7HMqDnJw)  

[**Spritesheets**](https://youtu.be/ePiMYe7JpJo)  
[**Tiles and Tile Maps**](https://youtu.be/37phHwLtaFg)  

I also found some tools for online asset creation that can make game development easier. Pixilart Online is where you create individual sprites.
Spritesheet Packer is the online version of TexturePacker a program that automatically creates a spritesheet with a corresponding JSON metadata file.
I don't know how adding more sprites later on would affect the output (maybe a solid reason to download the actual program.)
BeepBox is an online music creation tool that I used to make the .wav music files. 
BFXR is an online sound effects creation tool that can automatically generate common sound effects.
Bosca Ceoil is another music creation tool that is more robust than BeepBox.
Aseprite is the premier software for drawing and animating pixel art.
Tiled is a tile based map editor that can output as a .csv file to be read by python.

Links:  
[**Pixilart Online**](https://www.pixilart.com/draw)  
[**Spritesheet Packer**](https://www.codeandweb.com/free-sprite-sheet-packer)  
[**BeepBox**](https://www.beepbox.co/)  
[**BFXR**](https://www.bfxr.net/)  
[**Bosca Ceoil**](https://boscaceoil.net/)  
[**Aseprite**](https://www.aseprite.org/)  
[**Tiled**](https://www.mapeditor.org/)  

Youtubers:  
[**Tech With Tim**](https://www.youtube.com/channel/UC4JX40jDee_tINbkjycV4Sg)  
[**DaFluffyPotato**](https://www.youtube.com/channel/UCYNrBrBOgTfHswcz2DdZQFA)  
[**Christian Duenas**](https://www.youtube.com/channel/UCB2mKxxXPK3X8SJkAc-db3A)

[**PyGame Docs**](https://www.pygame.org/docs/)

Potential Frameworks to Pull Inspiration from:  
[**Pygin**](https://github.com/CarlosMatheus/Pygin)  
[**Pygame Functions**](https://github.com/StevePaget/Pygame_Functions)  
[**DaFlufflyPotato Engine**](https://gitlab.com/RichardMartinez/itp100/-/blob/master/programs/RnD/gameDev/template/DFPengine.py)    
Please note that while the DaFluffyPotato Engine is in my repo, it is NOT my engine. It is there because there was not an existing page of the file on GitHub.  

[**Zelle Tkinter Graphics**](https://mcsp.wartburg.edu/zelle/python/graphics.py)  
[**GASP Pygame**](https://codeberg.org/GASP/GASP_Pygame/src/branch/main/gasp/color.py)  
[**GASP Tkinter**](https://codeberg.org/GASP/GASP_Tkinter/src/branch/main/gasp/gasp.py)  
