# Messing With Tables

I saw the formatted output of the Voting Paradox challenge problem on the ITP100 website and wanted to replicate it. I did some web searches and found a great piece of documentation on making clean looking tables using the .format() method for strings. This directory is where I tested and learned this new method.

Here is the documentation:
[**Part 1**](https://scientificallysound.org/2016/10/06/python-print/)
[**Part 2**](https://scientificallysound.org/2016/10/10/python-print2/)
[**Part 3**](https://scientificallysound.org/2016/10/17/python-print3/)
