def check(elems):
    if len(elems) == len(set(elems)):
        return False
    return True

test = [1, 1, 2, 3, 5]
test2 = [1, 2, 3, 4, 10]

if check(test):
    print('List 1 has a match')

if check(test2):
    print('List 2 has a match')
