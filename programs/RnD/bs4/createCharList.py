from urllib.request import urlopen
from bs4 import BeautifulSoup as bSoup

# Url on Ultimate Frame Data for Joker
my_url = "https://ultimateframedata.com"

# Open URL, read HTML, close connection
client = urlopen(my_url)
page_html = client.read()
client.close()

# HTML Parsing
page_soup = bSoup(page_html, "html.parser")

name_containers = page_soup.find_all("div", {"class": "charactericon"})
# name_containers[0] is stats page

file = open("charList.txt", "w", encoding="utf-8")

for container in name_containers[1:]:
    char_url = container.a["href"][1:-4]

    file.write(char_url + "\n")

file.close()
