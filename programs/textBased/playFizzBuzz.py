# How to play Fizz Buzz: Start counting from 1. 
# For every multiple of 3, say 'Fizz'. 
# For every multiple of 5, say 'Buzz'. 
# If both, say 'FizzBuzz'.
# If neither, say the number. 
# Taken from a Tom Scott Youtube Video.
# Now adapted to take any number of input factors and output strings.
# Order of nums list matches order of strings list.

import sys

nums = input('What should the factors be? (Separate by a space) ')
nums = [int(x) for x in nums.split()]

strings = input('What should each factor be replaced by? (Separate by a space) ')
strings = strings.split()

if len(nums) != len(strings):
    sys.exit('Please make sure the factors and strings match one to one.')

limit = int(input('What should the max number be? '))


for i in range(1, limit + 1):
    output = ''
    for d in nums:
        if i % d == 0:
            output += strings[nums.index(d)]
    if output != '':
        print(f"{output} ({i})")
    else:
        print(i)
