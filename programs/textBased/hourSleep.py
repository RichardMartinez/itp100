# Imports
import math
import sys


# Define Between Function
def between(i):
    switcher = {
        0: '0-2',
        1: '3-5',
        2: '6-13',
        3: '14-17',
        4: '18+'
    }
    return switcher.get(i, 'INVALID BETWEEN')


# Define Required Function
def required(i):
    switcher = {
        0: '11-14',
        1: '10-13',
        2: '9-11',
        3: '8-10',
        4: '7-9'
    }
    return switcher.get(i, 'INVALID REQUIRED')


# Pure Time to Separate Numbers
def convert(string):
    li = list(string.split(":"))
    return li


# Initial Parameters
sleep = str(input('What time did you go to sleep last night? (24H)'))
wake = str(input('Ok, and what time did you wake up this morning? (24H)'))
age = int(input('And how old are you?'))
sleepList = convert(sleep)
wakeList = convert(wake)

# Check for Invalid Times
if int(sleepList[0]) > 24 or int(wakeList[0]) > 24 or int(sleepList[1]) > 60 or int(wakeList[1]) > 60:
    sys.exit('Please enter a valid time. The hours must be in the range 0 to 24 and the minutes must be in the range 0 to 60')


# Determine Hours of Sleep
minutes = int(wakeList[1]) - int(sleepList[1]) + 60
if minutes >= 60:
    minutes = minutes - 60
hours = int(wakeList[0]) - int(sleepList[0]) + 24
if hours > 24:
    hours = hours - 24

# Hour Correction
if int(wakeList[1]) - int(sleepList[1]) < 0:
    hours = hours - 1


# Age Range Test
groups = [range(0, 3), range(3, 6), range(6, 14), range(14, 18), range(18, 99)]
for r in groups:
    if age in r:
        b = int(groups.index(r))
    if age >= 99:
        b = 4


# Required Sleep Test
hourRange = [range(11, 15), range(10, 14), range(9, 12), range(8, 11), range(7, 10)]
minimum = int(min(list(hourRange[b])))
maximum = int(max(list(hourRange[b])))
totalTime = hours + (minutes/60)

if minimum <= totalTime <= maximum:
    output = 'This means you slept within the range for your age.'
elif totalTime > maximum:
    deviation = totalTime - maximum
    devList = list(math.modf(deviation))
    devHours = int(devList[1])
    devMinutes = int(devList[0] * 60)
    output = 'This means you overslept your range by ' + str(devHours) + ' hours and ' + str(devMinutes) + ' minutes. Try waking up earlier.'
elif totalTime < minimum:
    deviation = minimum - totalTime
    devList = list(math.modf(deviation))
    devHours = int(devList[1])
    devMinutes = int(devList[0] * 60)
    output = 'This means you under slept your range by ' + str(devHours) + ' hours and ' + str(devMinutes) + ' minutes. Try going to bed earlier.'
else:
    output = "How did this happen?"


# Output
print('Your age is ' + str(age) + ' and you slept ' + str(hours) + ' hours and ' + str(minutes) + ' minutes last night.')
print('Since you are in the age range of ' + str(between(b)) + ', you should have slept ' + str(required(b)) + ' hours.')
print(output)
