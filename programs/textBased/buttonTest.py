import pygame
pygame.font.init()


# --- class ---
class Button(object):

    def __init__(self, position, size, color, score=0):
        font = pygame.font.SysFont("comicsans", 35)
        self.position = position
        self.score = score
        self.label = font.render(f"{self.score}", 1, (255, 255, 255))
        self.color = color

        # create 3 images
        self._images = [
            pygame.Surface(size),
            pygame.Surface(size),
            pygame.Surface(size),
        ]

        # fill images with color - red, green, blue
        self._images[0].fill((255,0,0))
        self._images[1].fill((0,255,0))
        self._images[2].fill((0,0,255))

        # get image size and position
        self._rect = pygame.Rect(position, size)

        # select first image
        COLOR_MAP = {
                    "red": 0,
                    "green": 1,
                    "blue": 2
        }
        self._index = COLOR_MAP[color]

    def draw(self, screen):

        # draw selected image
        screen.blit(self._images[self._index], self._rect)
        font = pygame.font.SysFont("comicsans", 35)
        self.label = font.render(f"{self.score}", 1, (255, 255, 255))
        screen.blit(self.label, (self.position[0], self.position[1]))

    def event_handler(self, event):

        # change selected color if rectange clicked
        if event.type == pygame.MOUSEBUTTONDOWN: # is some button clicked
            if event.button == 1: # is left button clicked
                if self._rect.collidepoint(event.pos): # is mouse over button
                    self._index = (self._index+1) % 3 # change image
                    self.score += 1

# --- main ---

# init

pygame.init()

screen = pygame.display.set_mode((320,110))

# create buttons

button1 = Button((5, 5), (100, 100), "red")
button2 = Button((110, 5), (100, 100), "green")
button3 = Button((215, 5), (100, 100), "blue")

# mainloop

running = True

while running:

    # --- events ---

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        # --- buttons events ---

        button1.event_handler(event)
        button2.event_handler(event)
        button3.event_handler(event)

    # --- draws ---

    screen.fill((0, 0, 0))
    button1.draw(screen)
    button2.draw(screen)
    button3.draw(screen)

    pygame.display.update()

# --- the end ---

pygame.quit()