# Text Based Programs

Here are all of the text based programs I have worked on. Some are very simple tests while others may be more complex. All text is printed to the console.

### hourSleep.py

This program asks you what time you went to sleep, what time you woke up, and your age. Enter the times as actual times using a colon (:) but times must be in 24 hour military time. For example, I went to sleep at 23:15 and woke up at 9:00. This program is accurate down to the minute. Based on this information, it will tell you whether or not you slept within the amount of hour recommended by scientists and how much exactly you over or underslept.

### playFizzBuzz.py

This program plays the children's game Fizz Buzz. To play this game, you start by counting up from 1. For every multiple of 3, say Fizz. For every multiple of 5, say Buzz. For every multiple of both, say FizzBuzz. It is a little complicated but it is explained much better in [**this**](https://youtu.be/QPZ0pIK_wsc) Tom Scott youtube video which was where I got the idea from. I adapted this idea, as suggested in the video, to make the program work for any number of factors and strings. Separate each factor or string with a space and make sure the factors and strings line up correctly. Input how far you want the program to count and it will output the correct sequence of numbers and strings counting up to your limit.

### hello.py and macTest.py

These are small simple tests that I wrote when first starting to code. They don't do much but it is nice to see where I started out.
