# Inspired by Cookie Clicker Game
# First full game made by me not following a tutorial
# Using PyGame Module

import pygame
import os
import sys
pygame.init()
pygame.font.init()

# Init Screen
WIDTH, HEIGHT = 850, 750
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Cookie Clicker")
FPS = 60
clock = pygame.time.Clock()

# Init Pos.
cookie_pos = (200, 200)
cookie_size = (450, 450)
shop_pos = (730, 20)
shop_size = (100, 100)
buy_size = (256, 134)
CPS = 0

# Colors
TAN = (255, 206, 119)
BLUE = (67, 171, 250)
BLACK = (0, 0, 0)

# Load Images
BG = pygame.transform.scale(pygame.image.load(os.path.join("assets", "cloud_background.jpg")), (WIDTH, HEIGHT))
COOKIE_IMG = pygame.transform.scale(pygame.image.load(os.path.join("assets", "cookie.png")), cookie_size)
SHOP_IMG = pygame.Surface(shop_size)
SHOP_IMG.fill(BLUE)
ITEM_IMG = pygame.Surface(buy_size)
ITEM_IMG.fill(BLUE)


# Parent Button Class
class Button:
    def __init__(self, position, size, img, msg):
        self.position = position
        self.size = size
        self.img = img
        self.rect = pygame.Rect(position, size)
        self.label = None
        self.msg = msg

    def draw(self, window):
        window.blit(self.img, self.rect)

    # If button is clicked, return True
    def event_handler(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                if self.rect.collidepoint(event.pos):
                    return True


# Main Cookie Button
class Cookie(Button):
    font = pygame.font.SysFont("comicsans", 80)
    counter = 0
    n = 0

    def __init__(self, position, size, img, msg='', score=0):
        super().__init__(position, size, img, msg)
        self.score = score

    # Draw Self and Assigned Text Label
    def draw(self, window):
        window.blit(self.img, self.rect)
        self.msg = f"Cookies: {'{:,}'.format(self.score)}"
        self.label = self.font.render(self.msg, 1, TAN)
        window.blit(self.label, (int(WIDTH/2 - self.label.get_width()/2), 20))

    def update_score(self, window):
        global CPS
        self.n = 0
        for item in items:
            CPS = item.output * item.num_owned
            self.n += CPS

        if self.counter < FPS:
            self.counter += 1
            if self.counter == FPS:
                self.score += self.n
                self.counter = 0

        n_label = self.font.render(f"¢PS: {'{:,}'.format(self.n)}", 1, TAN)
        window.blit(n_label, (int(WIDTH/2 - n_label.get_width()/2), 80))


# Main Shop Button
class Shop(Button):
    font = pygame.font.SysFont("comicsans", 25)

    def __init__(self, position, size, img, msg, price, output, num_owned=0):
        super().__init__(position, size, img, msg)
        self.num_owned = num_owned
        self.price = price
        self.output = output

    # Draw Self and Assigned Text Label with Border
    def draw(self, window):
        border_width = 2
        border = pygame.Surface((self.size[0]+2*border_width, self.size[1]+2*border_width))
        border.fill(BLACK)
        window.blit(border, (self.position[0]-border_width, self.position[1]-border_width))
        window.blit(self.img, self.rect)
        if self.msg != "Open Shop":
            self.font = pygame.font.SysFont("comicsans", 23)
            self.label = self.font.render(f"({self.num_owned}) {self.msg}", 1, TAN)
        else:
            self.label = self.font.render(self.msg, 1, TAN)
        window.blit(self.label, (int(self.position[0]+self.size[0]/2-self.label.get_width()/2), int(self.position[1]+self.size[1]/2-self.label.get_height()/2)))
        # print(self.num_owned)
        # print(self.label.get_width(), self.label.get_height())


def shop_map(c, r):
    pos = (20 + c*(256+20), r*(134+20))
    return pos


# Define Button Objects
COOKIE = Cookie(cookie_pos, cookie_size, COOKIE_IMG)
SHOP = Shop(shop_pos, shop_size, SHOP_IMG, "Open Shop", 0, 0)
Cursor = Shop(shop_map(0, 0), buy_size, ITEM_IMG, "Buy Cursor (¢50 > 5 ¢PS)", 50, 5)
Employee = Shop(shop_map(0, 1), buy_size, ITEM_IMG, "Buy Employee (¢200 > 10 ¢PS)", 200, 10)
Bakery = Shop(shop_map(0, 2), buy_size, ITEM_IMG, "Buy Bakery (¢500 > 30 ¢PS)", 500, 30)
Shipment = Shop(shop_map(0, 3), buy_size, ITEM_IMG, "Buy Shipment (¢1k > 50 ¢PS)", 1000, 50)
Factory = Shop(shop_map(0, 4), buy_size, ITEM_IMG, "Buy Employee (¢2k > 100 ¢PS)", 2000, 100)
Lot = Shop(shop_map(1, 0), buy_size, ITEM_IMG, "Buy Lot (¢5k > 500 ¢PS)", 5000, 500)
Inc = Shop(shop_map(1, 1), buy_size, ITEM_IMG, "Buy Cookie Inc. (¢10k > 2k ¢PS)", 10_000, 2000)
CEO = Shop(shop_map(1, 2), buy_size, ITEM_IMG, "Buy CEO (¢50k > 10k ¢PS)", 50_000, 10_000)
Gold = Shop(shop_map(1, 3), buy_size, ITEM_IMG, "Buy Gold Bar (¢150k > 20k ¢PS)", 150_000, 20_000)
Bank = Shop(shop_map(1, 4), buy_size, ITEM_IMG, "Buy Bank (¢300k > 50k ¢PS)", 300_000, 50_000)
Alien = Shop(shop_map(2, 0), buy_size, ITEM_IMG, "Buy Alien (¢500k > 100k ¢PS)", 500_000, 100_000)
UFO = Shop(shop_map(2, 1), buy_size, ITEM_IMG, "Buy UFO (¢1M > 200k ¢PS)", 1_000_000, 200_000)
Fleet = Shop(shop_map(2, 2), buy_size, ITEM_IMG, "Buy Fleet (¢1.25M > 300k ¢PS)", 1_250_000, 300_000)
Planet = Shop(shop_map(2, 3), buy_size, ITEM_IMG, "Buy Planet (¢3M > 500k ¢PS)", 3_000_000, 500_000)
Civilization = Shop(shop_map(2, 4), buy_size, ITEM_IMG, "Buy Civilization (¢5M > 1M ¢PS)", 5_000_000, 1_000_000)

items = [Cursor, Employee, Bakery, Shipment, Factory, Lot, Inc, CEO, Gold, Bank, Alien, UFO, Fleet, Planet, Civilization]


# Main Game Loop
def main():
    run1 = True
    global FPS
    global clock

    def redraw_window():
        # Draw Background
        WIN.blit(BG, (0, 0))
        COOKIE.draw(WIN)
        SHOP.draw(WIN)

        COOKIE.update_score(WIN)

        pygame.display.update()

    while run1:
        clock.tick(FPS)
        redraw_window()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run1 = False

            if COOKIE.event_handler(event):
                COOKIE.score += 1
            if SHOP.event_handler(event):
                open_shop()
        if COOKIE.score >= 1_000_000_000:
            congrats()


def open_shop():
    run2 = True
    global FPS
    global clock

    def redraw_window():
        # Draw Background
        WIN.blit(BG, (0, 0))

        for item in items:
            item.draw(WIN)

        score_label = Shop.font.render(f"Cookies: {'{:,}'.format(COOKIE.score)}", 1, TAN)
        WIN.blit(score_label, (int(WIDTH/2 - score_label.get_width()/2), 30))

        pygame.display.update()

    while run2:
        clock.tick(FPS)
        redraw_window()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run2 = False

            for item in items:
                if item.event_handler(event) and COOKIE.score >= item.price:
                    item.num_owned += 1
                    COOKIE.score -= item.price


def main_menu():
    run3 = True
    global FPS
    global clock

    def redraw_window():
        # Draw Background
        WIN.blit(BG, (0, 0))

        title_label1 = Cookie.font.render(f"Reach ¢1B Cookies to Win!", 1, TAN)
        WIN.blit(title_label1, (int(WIDTH/2 - title_label1.get_width()/2), int(HEIGHT/2 - title_label1.get_height()/2 - title_label1.get_height())))
        title_label2 = Cookie.font.render(f"Click to begin...", 1, TAN)
        WIN.blit(title_label2, (int(WIDTH/2 - title_label2.get_width()/2), int(HEIGHT/2 - title_label2.get_height()/2 + title_label2.get_height())))

        pygame.display.update()

    while run3:
        clock.tick(FPS)
        redraw_window()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    main()


def congrats():
    run4 = True
    global FPS
    global clock

    def redraw_window():
        # Draw Background
        WIN.blit(BG, (0, 0))

        title_label1 = Cookie.font.render(f"You reached ¢1B Cookies!", 1, TAN)
        WIN.blit(title_label1, (int(WIDTH / 2 - title_label1.get_width() / 2), int(HEIGHT / 2 - title_label1.get_height() / 2 - title_label1.get_height())))
        title_label2 = Cookie.font.render(f"Congrats! You won!", 1, TAN)
        WIN.blit(title_label2, (int(WIDTH / 2 - title_label2.get_width() / 2), int(HEIGHT / 2 - title_label2.get_height() / 2 + title_label2.get_height())))

        pygame.display.update()

    while run4:
        clock.tick(FPS)
        redraw_window()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()


main_menu()
