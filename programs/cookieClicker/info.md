# Cookie Clicker
#### Using PyGame

This is the first full game made by me not following a youtube tutorial. It is inspired by the real Cookie Clicker game. Your goal is to reach 1 billion cookies. Every time you click the giant cookie in the center of the screen, you gain 1 cookie. With enough cookies, you can open the shop in the top right corner and buy items. Each item has a cost and an output. You buy the cookie for its cost and as a reward, you will automatically gain its output in cookies per second. Your cookies per second is also displayed on the main screen. You do not gain automatic cookies while in the shop. You press ESC to go back in the menus and the title screen also doubles as a pause screen.
