# Platformer
#### Using PyGame

This game is a simple one screen platformer. It has physics and pulls its game map and assets from its own asset folder. All assets including the main theme song was all made by me using online tools. This game was made following [**this**](https://www.youtube.com/playlist?list=PLX5fBCkxJmm1fPSqgn9gyR3qih8yYLvMj) youtube tutorial series. I have not yet completed the series (only up to video 4) but I plan to finish the series and the game moving forward. Use WASD to move and the spacebar or W to jump. Hold down either jump button for a higher jump, or press it quickly for a shorter jump. All player movement values can be easily changed for testing. This is the most complex game I have made in PyGame.
