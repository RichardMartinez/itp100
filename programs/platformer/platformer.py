# Imports
import pygame
import sys
pygame.init()
pygame.font.init()


# Define Global Functions
def load_img(path):
    global scalar
    img = pygame.image.load(path)
    img = pygame.transform.scale(img, (img.get_width()*scalar, img.get_height()*scalar))
    return img


def load_game_map():
    game_map = []
    with open("assets/game_map.txt", "r") as f:
        for line in f:
            line = line.rstrip()
            game_map.append(list(line))
    return game_map


def return_sign(num):
    if num >= 0:
        sign = 1
    else:
        sign = -1
    return sign


def integerize(obj):
    if type(obj) == list:
        return [int(x) for x in obj]
    elif type(obj) == float or type(obj) == str or type(obj) == int:
        return int(obj)


# Init
# Init Screen
WIDTH, HEIGHT = 744, 408
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Platformer")
FPS = 60
clock = pygame.time.Clock()

# Load Images
scalar = 3
player_img = load_img("assets/player.png")
grass_img = load_img("assets/grass.png")
dirt_img = load_img("assets/dirt.png")


# Colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
SKY_BLUE = (110, 222, 255)

# Load and Play Music Indefinitely
pygame.mixer.music.load("assets/Jungle Theme.wav")
pygame.mixer.music.play(-1)


# Player Class
class Player:
    gravity = 0.2
    fall_speed = 0
    vel = [0, 0]
    img = player_img

    def __init__(self, x=15, y=6):
        self.collision_types = {'top': False, 'bottom': False, 'right': False, 'left': False}
        self.x = x*tile_size
        self.y = y*tile_size
        self.rect = pygame.Rect(int(self.x), int(self.y), self.img.get_width(), self.img.get_height())

    def draw(self):
        # self.update_img()

        self.x = int(self.x)
        self.y = int(self.y)

        self.collisions()
        self.update_rect()

        # pygame.draw.rect(WIN, WHITE, self.rect)
        WIN.blit(self.img, (int(self.x), int(self.y)))

    def update_rect(self):
        self.rect = pygame.Rect(int(self.x), int(self.y), self.img.get_width(), self.img.get_height())

    def gen_hit_list(self):
        hit_list = []
        for tile in tiles:
            if self.rect.colliderect(tile.rect):
                hit_list.append(tile)
        return hit_list

    def collisions(self):
        self.collision_types = {'top': False, 'bottom': False, 'right': False, 'left': False}

        # Update X with respect to Edges
        if self.x + self.vel[0] > 0 and self.x + self.img.get_width() + self.vel[0] < WIDTH:
            self.x += self.vel[0]
        else:
            if self.vel[0] > 0:
                self.x = WIDTH - self.img.get_width()
            elif self.vel[0] < 0:
                self.x = 0
            self.vel[0] = 0

        # Test for Collisions
        self.update_rect()
        for tile in self.gen_hit_list():
            if self.vel[0] > 0:  # <= tile.rect.right:
                self.x = tile.rect.left - self.img.get_width()
                self.collision_types['right'] = True
                self.vel[0] = 0
            elif self.vel[0] < 0:
                self.x = tile.rect.right
                self.collision_types['left'] = True

        # Update Y and Test for Collisions
        self.y += self.vel[1]
        self.update_rect()
        for tile in self.gen_hit_list():
            if self.vel[1] > 0:  # or self.y + self.img.get_height() >= tile.rect.top:
                self.y = tile.rect.top - PLAYER.img.get_height()
                self.collision_types['bottom'] = True
                self.vel[1] = 0
            elif self.vel[1] < 0:  # or self.y <= tile.rect.bottom:
                self.y = tile.rect.bottom
                self.collision_types['top'] = True
                self.fall_speed = 0

    def move(self):
        # Define Max Velocities
        max_x_p_vel, max_down_p_vel, max_up_p_vel, max_fall_speed = 5, 15, -10, 2
        x_accel_up, x_accel_down = 0.5, 1

        # Pull Down Based on Gravity
        self.vel[1] += self.fall_speed
        self.fall_speed += self.gravity
        if self.fall_speed > max_fall_speed:
            self.fall_speed = max_fall_speed
        if self.vel[1] < 0:
            self.vel[1] += 1

        # Keep Vertical Velocity within Limits
        if self.vel[1] > max_down_p_vel:
            self.vel[1] = max_down_p_vel
        elif self.vel[1] < max_up_p_vel:
            self.vel[1] = max_up_p_vel

        # Make Dictionary that Contains Key Presses True or False
        keys = pygame.key.get_pressed()
        movement = {'right': bool(keys[pygame.K_d]), 'left': bool(keys[pygame.K_a]), 'up': bool(keys[pygame.K_w]),
                    'down': bool(keys[pygame.K_s])}

        # Control X Movement
        slowing_x = (not (movement['left'] or movement['right'])) or (movement['left'] and movement['right'])
        if slowing_x:
            if self.vel[0] > 0:
                self.vel[0] -= x_accel_down
            if self.vel[0] < 0:
                self.vel[0] += x_accel_down
        else:
            if movement['left']:
                self.vel[0] -= x_accel_up
            elif movement['right']:
                self.vel[0] += x_accel_up

            # Keep Horizontal Velocity within Limits
            if abs(self.vel[0]) > max_x_p_vel:
                self.vel[0] = max_x_p_vel * return_sign(self.vel[0])

        # Fast Fall
        if movement['down'] and abs(self.vel[1]) < max_down_p_vel:
            self.vel[1] += 1

        # Bonk on Blocks
        if self.collision_types['right'] or self.collision_types['left']:
            self.vel[0] = 0
        if self.collision_types['top']:  # or self.collision_types['bottom']:
            self.vel[1] = 0

        # Quit on X and Test for Jumps
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if (event.key == pygame.K_SPACE or event.key == pygame.K_w) and self.collision_types['bottom']:
                    self.fall_speed = -3.25
                if event.key == pygame.K_ESCAPE:
                    sys.exit()

        # Short Hop
        if not (keys[pygame.K_SPACE] or keys[pygame.K_w]):
            self.fall_speed += 0.2

    # def update_img(self):
    #     global flip
    #     if self.vel[0] > 0:
    #         flip = False
    #     elif self.vel[0] < 0:
    #         flip = True
    #
    #     self.img = pygame.transform.flip(self.img, flip, False)


# Tile Class
class Tile:
    def __init__(self, img, x, y):
        self.img = img
        self.x = x*tile_size
        self.y = y*tile_size
        self.rect = pygame.Rect(self.x, self.y, tile_size, tile_size)

    def draw(self):
        WIN.blit(self.img, (self.x, self.y))


# Create Tiles from Game Map Text File
GAME_MAP = load_game_map()


def create_tiles():
    # Create Tiles List and Fill with Tile Objects
    global tiles
    tiles = []
    map_y = 0
    for row in GAME_MAP:
        map_x = 0
        for tile in row:
            if tile == '1':
                tiles.append(Tile(dirt_img, map_x, map_y))
            elif tile == '2':
                tiles.append(Tile(grass_img, map_x, map_y))
            map_x += 1
        map_y += 1
    return tiles


tile_size = grass_img.get_width()  # = 24
tiles = create_tiles()


# Define Player Object
PLAYER = Player()


# Main Game Loop
def main():
    run = True
    global clock
    global FPS

    def redraw_window():
        WIN.fill(SKY_BLUE)

        # Draw All Tiles
        for tile in tiles:
            tile.draw()

        PLAYER.draw()

        pygame.display.update()

    while run:
        clock.tick(FPS)

        PLAYER.move()

        # Test Print
        # print(integerize(PLAYER.vel), int(PLAYER.fall_speed))

        redraw_window()


main()
